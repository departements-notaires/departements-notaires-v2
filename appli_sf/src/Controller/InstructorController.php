<?php

namespace App\Controller;

use App\Entity\Instructor;
use App\Form\InstructorType;
use App\Helper\MessageHelper;
use App\Repository\InstructorRepository;
use App\Repository\PersonRepository;
use App\Service\SettingsDataService;
use App\Service\ThemeService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * @Route("/admin")
 */
class InstructorController extends BaseController
{
    public function __construct(
        protected string $appTerritoryInstructor,
        protected EntityManagerInterface $entityManager,
        ThemeService $service,
        SettingsDataService $settingsDataServiceService,
    ) {
        parent::__construct($settingsDataServiceService, $service);
    }

    /**
     * Liste les instructeurs.
     */
    #[Route('/instructor/list', name: 'admin_instructor_list')]
    public function listAction(
        InstructorRepository $userRepository,
        PersonRepository $personRepository,
    ): Response {
        $classicInstructors = $userRepository->findAll();
        $territoryInstructors = $personRepository->findTerritoryInstructors();

        return $this->render(
            'admin/instructor/list.html.twig',
            [
                'classic_instructors' => $classicInstructors,
                'territory_instructors' => $territoryInstructors,
                'has_territory_instructors' => $this->appTerritoryInstructor,
            ]
        );
    }

    /**
     * Ajoute un instructeur.
     */
    #[Route('/instructor/add', name: 'admin_instructor_add')]
    public function addAction(
        Request $request,
        TranslatorInterface $translator
    ): Response {
        $instructor = new Instructor();

        $form = $this->createForm(InstructorType::class, $instructor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($instructor);
            $this->entityManager->flush();

            return $this->redirectToRoute('admin_instructor_list');
        }

        return $this->render(
            'admin/instructor/add.html.twig',
            [
                'instructorForm' => $form->createView(),
            ]
        );
    }

    /**
     * Édite un instructeur.
     */
    #[Route('/instructor/{id}/edit', name: 'admin_instructor_edit')]
    public function editAction(
        Instructor $instructor,
        Request $request,
        TranslatorInterface $translator
    ): Response {
        $form = $this->createForm(InstructorType::class, $instructor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->entityManager->persist($instructor);
            $this->entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $translator->trans('instructorEdit.updated'));

            return $this->redirectToRoute(
                'admin_instructor_list',
                [
                    'id' => $instructor->getId(),
                ]
            );
        }

        return $this->render(
            'admin/instructor/edit.html.twig',
            [
                'instructorForm' => $form->createView(),
            ]
        );
    }

    /**
     * Supprime un instructeur.
     */
    #[Route('/instructor/{id}/remove', name: 'admin_instructor_remove')]
    public function removeAction(Request $request, Instructor $instructor, TranslatorInterface $translator): RedirectResponse
    {
        $submittedToken = $request->request->get('token');

        if ($this->isCsrfTokenValid('delete-instructor', $submittedToken)) {
            $this->entityManager->remove($instructor);
            $this->entityManager->flush();

            $this->addFlash(MessageHelper::MESSAGE_SUCCESS, $translator->trans('instructorDelete.success'));
        } else {
            $this->addFlash(MessageHelper::MESSAGE_ERROR, $translator->trans('instructorDelete.error'));
        }

        return $this->redirectToRoute('admin_instructor_list');
    }
}
