<?php

namespace App\Form;

use App\Data\SettingsData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Count;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

class SettingsType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    /**
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws \ReflectionException
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $typeResponseForDocument = explode(',', $options['data']->getAnswerCallDocument());
        if (count($typeResponseForDocument) > 3) {
            $parameterTypeResponse = [implode(',', array_slice($typeResponseForDocument, 0, 3)),
                $typeResponseForDocument[3]];
        } elseif (3 === count($typeResponseForDocument)) {
            $parameterTypeResponse = [implode(',', array_slice($typeResponseForDocument, 0, 3))];
        } else {
            $parameterTypeResponse = $typeResponseForDocument;
        }
        $imageConstraint = [
            new File(
                [
                    'maxSize' => '2M',
                    'mimeTypes' => [
                        'image/png',
                        'image/jpeg',
                        'image/gif',
                        'image/svg+xml',
                        'image/svg',
                    ],
                    'mimeTypesMessage' => $this->translator->trans('settings.error.imageFile'),
                ]
            ),
        ];

        $pdfImageConstraint = [
            new File(
                [
                    'maxSize' => '1M',
                    'mimeTypes' => [
                        'image/png',
                        'image/jpeg',
                        'image/gif',
                    ],
                    'mimeTypesMessage' => $this->translator->trans('settings.error.pdfImageFile'),
                ]
            ),
        ];
        $builder
            ->add(
                'appName',
                TextType::class,
                [
                    'required' => true,
                    'label' => 'settings.appName',
                ]
            )
            ->add(
                'appUrl',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'settings.appUrl',
                    'empty_data' => '',
                ]
            )
            ->add(
                'appLogo',
                FileType::class,
                [
                    'required' => false,
                    'label' => 'settings.appLogo',
                    'constraints' => $imageConstraint,
                ]
            )
            ->add(
                'appFavicon',
                FileType::class,
                [
                    'required' => false,
                    'label' => 'settings.appFavicon',
                    'constraints' => $imageConstraint,
                ]
            )
            ->add(
                'departmentName',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'settings.departmentName',
                    'empty_data' => '',
                ]
            )
            ->add(
                'departmentSite',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'settings.departmentSite',
                    'empty_data' => '',
                ]
            )
            ->add(
                'departmentCilSite',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'settings.departmentCilSite',
                    'empty_data' => '',
                ]
            )
            ->add(
                'soundexSearch',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'settings.soundexSearch',
                ]
            )
            ->add(
                'ignoreDifferentFirstNameSearch',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'settings.ignoreDifferentFirstNameSearch',
                ]
            )
            ->add(
                'connectionAttempts',
                IntegerType::class,
                [
                    'required' => false,
                    'label' => 'settings.connectionAttempts',
                    'attr' => ['min' => 0, 'max' => 10],
                    'empty_data' => 3,
                ]
            )
            ->add(
                'acceptErrorInBirthDate',
                ChoiceType::class,
                [
                    'choices' => [
                        'settings.acceptErrorInBirthDate.1' => 1,
                        'settings.acceptErrorInBirthDate.2' => 2,
                        'settings.acceptErrorInBirthDate.3' => 3,
                        'settings.acceptErrorInBirthDate.4' => 4,
                        'settings.acceptErrorInBirthDate.5' => 5,
                    ],
                    'multiple' => false,
                    'expanded' => true,
                    'required' => true,
                    'label' => 'settings.acceptErrorInBirthDate.label',
                ]
            )
            ->add(
                'searchByName',
                ChoiceType::class,
                [
                    'choices' => [
                        'settings.searchByName.1' => 1,
                        'settings.searchByName.2' => 2,
                    ],
                    'multiple' => false,
                    'expanded' => true,
                    'required' => true,
                    'label' => 'settings.searchByName.label',
                ]
            )
            ->add(
                'documentaryExchange',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'settings.documentaryExchange',
                ]
            )
            ->add(
                'answerCallDocumentTemp',
                ChoiceType::class,
                [
                    'choices' => [
                        'settings.answerCallDocument.1' => '1,5,2',
                        'settings.answerCallDocument.2' => 4,
                    ],
                    'multiple' => true,
                    'expanded' => true,
                    'required' => true,
                    'mapped' => false,
                    'label' => 'settings.answerCallDocument.label',
                    'constraints' => [
                        new Count([
                            'min' => 1,
                            'minMessage' => 'Au moins une case doit être cochée',
                        ]),
                    ],
                    'data' => $parameterTypeResponse,
                ]
            )
            ->add(
                'sendMailDocumentaryExchange',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'settings.sendMailDocumentaryExchange',
                ]
            )
            ->add(
                'answerCallDocument',
                HiddenType::class,
                [
                    'required' => false,
                ]
            )
            ->add(
                'deathAlert',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'settings.deathAlert',
                ]
            )->add(
                'deathAlertMail',
                EmailType::class,
                [
                    'required' => false,
                    'label' => 'settings.deathAlertMail',
                    'constraints' => new Email(),
                ]
            )
            ->add(
                'mailSignature',
                TextareaType::class,
                [
                    'required' => false,
                    'label' => 'settings.mailSignature',
                    'empty_data' => '',
                ]
            )
            ->add(
                'mailIntroduction',
                TextareaType::class,
                [
                    'required' => false,
                    'label' => 'settings.mailIntroduction',
                    'empty_data' => '',
                ]
            )
            ->add(
                'mailNotaBene',
                TextareaType::class,
                [
                    'required' => false,
                    'label' => 'settings.mailNotaBene',
                    'empty_data' => '',
                ]
            )
            ->add(
                'claims',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'settings.claims',
                ]
            )
            ->add(
                'claimsRecoverableStep',
                IntegerType::class,
                [
                    'required' => false,
                    'label' => 'settings.claimsRecoverableStep',
                ]
            )
            ->add(
                'claimsNonRecoverableStep',
                IntegerType::class,
                [
                    'required' => false,
                    'label' => 'settings.claimsNonRecoverableStep',
                ]
            )
            ->add(
                'claimsMail',
                EmailType::class,
                [
                    'required' => false,
                    'label' => 'settings.claimsMail',
                ]
            )
            ->add(
                'claimsMailManagement',
                EmailType::class,
                [
                    'required' => false,
                    'label' => 'settings.claimsMailManagement',
                ]
            )
            ->add(
                'mailBusinessSignature',
                TextareaType::class,
                [
                    'required' => false,
                    'label' => 'settings.mailBusinessSignature',
                    'empty_data' => '',
                ]
            )
            ->add(
                'mailBusinessImgSignature',
                FileType::class,
                [
                    'required' => false,
                    'label' => 'settings.mailBusinessImgSignature',
                    'constraints' => $pdfImageConstraint,
                    'empty_data' => '',
                ]
            )
            ->add(
                'mailBusinessImgSignatureDeleted',
                CheckboxType::class,
                [
                    'mapped' => false,
                    'value' => 0,
                    'required' => false,
                    'attr' => ['hidden' => 'hidden'],
                    'label' => false,
                ]
            )
            ->add(
                'pdfDestinationCity',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'settings.pdfDestinationCity',
                    'empty_data' => '',
                ]
            )
            ->add(
                'pdfLogo1',
                FileType::class,
                [
                    'required' => false,
                    'label' => 'settings.pdfLogo1',
                    'constraints' => $pdfImageConstraint,
                ]
            )
            ->add(
                'pdfLogo2',
                FileType::class,
                [
                    'required' => false,
                    'label' => 'settings.pdfLogo2',
                    'constraints' => $pdfImageConstraint,
                ]
            )
            ->add(
                'pdfServiceLogo',
                FileType::class,
                [
                    'required' => false,
                    'label' => 'settings.pdfServiceLogo',
                    'constraints' => $pdfImageConstraint,
                ]
            )
            ->add(
                'appLogoDeleted',
                CheckboxType::class,
                [
                    'mapped' => false,
                    'value' => 0,
                    'required' => false,
                    'attr' => ['hidden' => 'hidden'],
                    'label' => false,
                ]
            )
            ->add(
                'appFaviconDeleted',
                CheckboxType::class,
                [
                    'mapped' => false,
                    'value' => 0,
                    'required' => false,
                    'attr' => ['hidden' => 'hidden'],
                    'label' => false,
                ]
            )
            ->add(
                'pdfSignatureDeleted',
                CheckboxType::class,
                [
                    'mapped' => false,
                    'value' => 0,
                    'required' => false,
                    'attr' => ['hidden' => 'hidden'],
                    'label' => false,
                ]
            )
            ->add(
                'pdfServiceLogoDeleted',
                CheckboxType::class,
                [
                    'mapped' => false,
                    'value' => 0,
                    'required' => false,
                    'attr' => ['hidden' => 'hidden'],
                    'label' => false,
                ]
            )
            ->add(
                'pdfLogo1Deleted',
                CheckboxType::class,
                [
                    'mapped' => false,
                    'value' => 0,
                    'required' => false,
                    'attr' => ['hidden' => 'hidden'],
                    'label' => false,
                ]
            )
            ->add(
                'pdfLogo2Deleted',
                CheckboxType::class,
                [
                    'mapped' => false,
                    'value' => 0,
                    'required' => false,
                    'attr' => ['hidden' => 'hidden'],
                    'label' => false,
                ]
            )
            ->add(
                'pdfSignature',
                FileType::class,
                [
                    'required' => false,
                    'label' => 'settings.pdfSignature',
                    'constraints' => $pdfImageConstraint,
                    'empty_data' => '',
                ]
            )
            ->add(
                'pdfSuccessionMail',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'settings.pdfSuccessionMail',
                    'empty_data' => '',
                ]
            )
            ->add(
                'pdfDepartmentalHouseName',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'settings.pdfDepartmentalHouseName',
                    'empty_data' => '',
                ]
            )
            ->add(
                'pdfDepartmentalHousePhone',
                TextType::class,
                [
                    'required' => false,
                    'label' => 'settings.pdfDepartmentalHousePhone',
                    'empty_data' => '',
                ]
            )
            ->add(
                'pdfCSS',
                FileType::class,
                [
                    'required' => false,
                    'label' => 'settings.pdfCSS',
                    'constraints' => [
                        new File(
                            [
                                'maxSize' => '100k',
                                'mimeTypes' => [
                                    'text/css',
                                    'text/plain',
                                ],
                                'mimeTypesMessage' => $this->translator->trans('settings.error.cssFile'),
                            ]
                        ),
                    ],
                ]
            )
            ->add(
                'serviceMessage',
                TextareaType::class,
                [
                    'required' => false,
                    'label' => 'settings.serviceMessage',
                    'empty_data' => '',
                ]
            )
            ->add(
                'instructorFilter',
                CheckboxType::class,
                [
                    'required' => false,
                    'label' => 'settings.instructorLabelFilter',
                ]
            );

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $data = $event->getData();
            $form = $event->getForm();
            $form->add(
                'deathAlertMail',
                EmailType::class,
                [
                    'required' => $data->hasDeathAlert() ?: false,
                    'label' => 'settings.deathAlertMail',
                    'empty_data' => '',
                ]
            );
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            $data = $event->getData();
            // On met en place ce fonctionnement car les paramètres sont de champ texte
            if (isset($data['answerCallDocumentTemp']) && is_array($data['answerCallDocumentTemp'])) {
                $data['answerCallDocument'] = implode(',', $data['answerCallDocumentTemp']);
                $event->setData($data);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => SettingsData::class,
            ]
        );
    }
}
