<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230612120614 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Modify log_recherche to add path of document';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE log_recherche ADD death_document VARCHAR(255) DEFAULT NULL, ADD net_document VARCHAR(255) DEFAULT NULL, ADD other_document VARCHAR(255) DEFAULT NULL, CHANGE nom_usage nom_usage VARCHAR(255) DEFAULT NULL, CHANGE prenom prenom LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', CHANGE lieu_deces lieu_deces VARCHAR(255) NOT NULL, CHANGE type_reponse type_reponse LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', CHANGE nom_civil nom_civil VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE log_recherche DROP death_document, DROP net_document, DROP other_document, CHANGE nom_usage nom_usage VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, CHANGE nom_civil nom_civil VARCHAR(255) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_general_ci`, CHANGE prenom prenom LONGTEXT CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci` COMMENT \'(DC2Type:json)\', CHANGE lieu_deces lieu_deces VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, CHANGE type_reponse type_reponse LONGTEXT CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci` COMMENT \'(DC2Type:json)\'');
    }
}
