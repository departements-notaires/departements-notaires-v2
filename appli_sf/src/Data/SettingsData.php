<?php

namespace App\Data;

use App\Annotations\BooleanSetting;
use App\Annotations\DontSave;
use App\Annotations\FileSetting;
use App\Annotations\ImageSetting;
use App\Entity\Settings;
use App\Helper\SearchHelper;
use App\Helper\UploaderHelper;
use App\Service\SettingService;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use ReflectionException;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SettingsData
{
    /**
     * @var string
     */
    protected $appName;

    protected $appUrl;

    /**
     * @var File
     *
     * @ImageSetting
     */
    protected $appLogo;

    /**
     * @var File
     *
     * @ImageSetting
     */
    protected $appFavicon;

    protected $departmentName;

    /**
     * @var string
     */
    protected $departmentSite;

    protected $departmentCilSite;

    /**
     * @BooleanSetting
     */
    protected $soundexSearch = false;

    /**
     * @BooleanSetting
     */
    protected $ignoreDifferentFirstNameSearch = false;

    protected $connectionAttempts = 3;

    protected $searchByName = SearchHelper::SEARCH_BY_USE_NAME;

    protected $acceptErrorInBirthDate = SearchHelper::SEARCH_DIFFERENT_DAY_AND_MONTH;

    /**
     * @BooleanSetting
     */
    protected $deathAlert = false;

    protected $deathAlertMail;

    protected $mailSignature;

    protected $mailIntroduction;

    protected $mailNotaBene;

    /**
     * @BooleanSetting
     */
    protected $claims = false;

    protected $claimsRecoverableStep = 0;

    protected $claimsNonRecoverableStep = 100;

    protected $claimsMail = 'mail-du-service-des-creances-recuperable@example.org';

    protected $claimsMailManagement = 'mail-du-service-de-gestion@example.org';

    protected $mailBusinessSignature;

    /**
     * @ImageSetting
     */
    protected $mailBusinessImgSignature;

    protected $pdfDestinationCity;

    /**
     * @ImageSetting
     */
    protected $pdfLogo1;

    /**
     * @ImageSetting
     */
    protected $pdfLogo2;

    /**
     * @ImageSetting
     */
    protected $pdfServiceLogo;

    /**
     * @ImageSetting
     */
    protected $pdfSignature;

    protected $pdfSuccessionMail;

    protected $pdfDepartmentalHouseName;

    protected $pdfDepartmentalHousePhone;

    /**
     * @FileSetting
     */
    protected $pdfCSS;

    /**
     * @DontSave
     */
    protected $settingService;

    /**
     * @DontSave
     */
    protected $uploaderHelper;

    /**
     * @BooleanSetting
     */
    protected $documentaryExchange = false;

    /**
     * @BooleanSetting
     */
    protected $sendMailDocumentaryExchange = true;

    protected $answerCallDocument = '1,5,2';

    /**
     * @var string
     */
    protected $serviceMessage = '';

    /**
     * @BooleanSetting
     */
    protected $instructorFilter = false;

    public function __construct(
        SettingService $settingService,
        ?UploaderHelper $uploaderHelper = null
    ) {
        $this->settingService = $settingService;
        $this->uploaderHelper = $uploaderHelper;
    }

    /**
     * @throws \Doctrine\Common\Annotations\AnnotationException
     * @throws ReflectionException
     */
    public static function getImagesProps(): array
    {
        $reader = new AnnotationReader();
        $reflClass = new \ReflectionClass(self::class);
        $props = $reflClass->getProperties();
        $imagesProps = [];

        foreach ($props as $prop) {
            $reflProp = new \ReflectionProperty(self::class, $prop->getName());
            $propertyAnnotations = $reader->getPropertyAnnotations($reflProp);

            if (self::hasObjectArray($propertyAnnotations, ImageSetting::class)) {
                $imagesProps[] = $prop->getName();
            }
        }

        return $imagesProps;
    }

    private static function hasObjectArray(array $array, string $classname): bool
    {
        foreach ($array as $element) {
            if ($element instanceof $classname) {
                return true;
            }
        }

        return false;
    }

    public function getAppName(): ?string
    {
        return $this->appName;
    }

    /**
     * @return $this
     */
    public function setAppName(string $appName): SettingsData
    {
        $this->appName = $appName;

        return $this;
    }

    public function getAppUrl(): ?string
    {
        return $this->appUrl;
    }

    /**
     * @return $this
     */
    public function setAppUrl(string $appUrl): SettingsData
    {
        $this->appUrl = $appUrl;

        return $this;
    }

    public function getAppLogo(): ?File
    {
        return $this->appLogo;
    }

    public function setAppLogo(?File $appLogo): SettingsData
    {
        if ($appLogo) {
            $this->appLogo = $appLogo;
        }

        return $this;
    }

    public function getAppFavicon(): ?File
    {
        return $this->appFavicon;
    }

    public function setAppFavicon(?File $appFavicon): SettingsData
    {
        if ($appFavicon) {
            $this->appFavicon = $appFavicon;
        }

        return $this;
    }

    public function getDepartmentName(): ?string
    {
        return $this->departmentName;
    }

    /**
     * @return $this
     */
    public function setDepartmentName(string $departmentName): SettingsData
    {
        $this->departmentName = $departmentName;

        return $this;
    }

    public function getDepartmentSite(): ?string
    {
        return $this->departmentSite;
    }

    /**
     * @return $this
     */
    public function setDepartmentSite(string $departmentSite): SettingsData
    {
        $this->departmentSite = $departmentSite;

        return $this;
    }

    public function getDepartmentCilSite(): ?string
    {
        return $this->departmentCilSite;
    }

    /**
     * @return $this
     */
    public function setDepartmentCilSite(string $departmentCilSite): SettingsData
    {
        $this->departmentCilSite = $departmentCilSite;

        return $this;
    }

    public function hasSoundexSearch(): ?bool
    {
        return $this->soundexSearch;
    }

    /**
     * @return $this
     */
    public function setSoundexSearch(bool $soundexSearch): SettingsData
    {
        $this->soundexSearch = $soundexSearch;

        return $this;
    }

    public function hasIgnoreDifferentFirstNameSearch(): ?bool
    {
        return $this->ignoreDifferentFirstNameSearch;
    }

    /**
     * @return $this
     */
    public function setIgnoreDifferentFirstNameSearch(bool $IgnoreDifferentFirstNameSearch): SettingsData
    {
        $this->ignoreDifferentFirstNameSearch = $IgnoreDifferentFirstNameSearch;

        return $this;
    }

    public function getConnectionAttempts(): int
    {
        return $this->connectionAttempts;
    }

    /**
     * @return $this
     */
    public function setConnectionAttempts(int $connectionAttempts): SettingsData
    {
        $this->connectionAttempts = $connectionAttempts;

        return $this;
    }

    public function getSearchByName(): int
    {
        return $this->searchByName;
    }

    /**
     * @return $this
     */
    public function setSearchByName(int $searchByName): SettingsData
    {
        $this->searchByName = $searchByName;

        return $this;
    }

    public function getAcceptErrorInBirthDate(): int
    {
        return $this->acceptErrorInBirthDate;
    }

    /**
     * @return $this
     */
    public function setAcceptErrorInBirthDate(int $acceptErrorInBirthDate): SettingsData
    {
        $this->acceptErrorInBirthDate = $acceptErrorInBirthDate;

        return $this;
    }

    public function hasDeathAlert(): bool
    {
        return $this->deathAlert;
    }

    /**
     * @return $this
     */
    public function setDeathAlert(bool $deathAlert): SettingsData
    {
        $this->deathAlert = $deathAlert;

        return $this;
    }

    public function getDeathAlertMail(): ?string
    {
        return $this->deathAlertMail;
    }

    /**
     * @return $this
     */
    public function setDeathAlertMail(string $deathAlertMail): SettingsData
    {
        $this->deathAlertMail = $deathAlertMail;

        return $this;
    }

    public function getMailSignature(): ?string
    {
        return $this->mailSignature;
    }

    /**
     * @return $this
     */
    public function setMailSignature(string $mailSignature): SettingsData
    {
        $this->mailSignature = $mailSignature;

        return $this;
    }

    public function getMailIntroduction(): ?string
    {
        return $this->mailIntroduction;
    }

    /**
     * @return $this
     */
    public function setMailIntroduction(string $mailIntroduction): SettingsData
    {
        $this->mailIntroduction = $mailIntroduction;

        return $this;
    }

    public function getMailNotaBene(): ?string
    {
        return $this->mailNotaBene;
    }

    /**
     * @return $this
     */
    public function setMailNotaBene(string $mailNotaBene): SettingsData
    {
        $this->mailNotaBene = $mailNotaBene;

        return $this;
    }

    public function hasClaims(): bool
    {
        return $this->claims;
    }

    /**
     * @return $this
     */
    public function setClaims(bool $claims): SettingsData
    {
        $this->claims = $claims;

        return $this;
    }

    public function getClaimsRecoverableStep(): int
    {
        return $this->claimsRecoverableStep;
    }

    /**
     * @return $this
     */
    public function setClaimsRecoverableStep(int $claimsRecoverableStep): SettingsData
    {
        $this->claimsRecoverableStep = $claimsRecoverableStep;

        return $this;
    }

    public function getClaimsNonRecoverableStep(): int
    {
        return $this->claimsNonRecoverableStep;
    }

    /**
     * @return $this
     */
    public function setClaimsNonRecoverableStep(int $claimsNonRecoverableStep): SettingsData
    {
        $this->claimsNonRecoverableStep = $claimsNonRecoverableStep;

        return $this;
    }

    public function getClaimsMail(): ?string
    {
        return $this->claimsMail;
    }

    /**
     * @return $this
     */
    public function setClaimsMail(string $claimsMail): SettingsData
    {
        $this->claimsMail = $claimsMail;

        return $this;
    }

    public function getClaimsMailManagement(): ?string
    {
        return $this->claimsMailManagement;
    }

    /**
     * @return $this
     */
    public function setClaimsMailManagement(string $claimsMailManagement): SettingsData
    {
        $this->claimsMailManagement = $claimsMailManagement;

        return $this;
    }

    public function getMailBusinessSignature(): ?string
    {
        return $this->mailBusinessSignature;
    }

    /**
     * @return $this
     */
    public function setMailBusinessSignature(string $mailBusinessSignature): SettingsData
    {
        $this->mailBusinessSignature = $mailBusinessSignature;

        return $this;
    }

    public function getMailBusinessImgSignature(): ?File
    {
        return $this->mailBusinessImgSignature;
    }

    public function setMailBusinessImgSignature(?File $mailBusinessImgSignature): SettingsData
    {
        if ($mailBusinessImgSignature) {
            $this->mailBusinessImgSignature = $mailBusinessImgSignature;
        }

        return $this;
    }

    public function getPdfDestinationCity(): ?string
    {
        return $this->pdfDestinationCity;
    }

    /**
     * @return $this
     */
    public function setPdfDestinationCity(string $pdfDestinationCity): SettingsData
    {
        $this->pdfDestinationCity = $pdfDestinationCity;

        return $this;
    }

    public function getPdfLogo1(): ?File
    {
        return $this->pdfLogo1;
    }

    public function setPdfLogo1(?File $pdfLogo1): SettingsData
    {
        if ($pdfLogo1) {
            $this->pdfLogo1 = $pdfLogo1;
        }

        return $this;
    }

    public function getPdfLogo2(): ?File
    {
        return $this->pdfLogo2;
    }

    public function setPdfLogo2(?File $pdfLogo2): SettingsData
    {
        if ($pdfLogo2) {
            $this->pdfLogo2 = $pdfLogo2;
        }

        return $this;
    }

    public function getPdfServiceLogo(): ?File
    {
        return $this->pdfServiceLogo;
    }

    public function setPdfServiceLogo(?File $pdfServiceLogo): SettingsData
    {
        if ($pdfServiceLogo) {
            $this->pdfServiceLogo = $pdfServiceLogo;
        }

        return $this;
    }

    public function getPdfSignature(): ?File
    {
        return $this->pdfSignature;
    }

    public function setPdfSignature(?File $pdfSignature): SettingsData
    {
        if ($pdfSignature) {
            $this->pdfSignature = $pdfSignature;
        }

        return $this;
    }

    public function getPdfSuccessionMail(): ?string
    {
        return $this->pdfSuccessionMail;
    }

    /**
     * @return $this
     */
    public function setPdfSuccessionMail(string $pdfSuccessionMail): SettingsData
    {
        $this->pdfSuccessionMail = $pdfSuccessionMail;

        return $this;
    }

    public function getPdfDepartmentalHouseName(): ?string
    {
        return $this->pdfDepartmentalHouseName;
    }

    /**
     * @return $this
     */
    public function setPdfDepartmentalHouseName(string $pdfDepartmentalHouseName): SettingsData
    {
        $this->pdfDepartmentalHouseName = $pdfDepartmentalHouseName;

        return $this;
    }

    public function getPdfDepartmentalHousePhone(): ?string
    {
        return $this->pdfDepartmentalHousePhone;
    }

    /**
     * @return $this
     */
    public function setPdfDepartmentalHousePhone(string $pdfDepartmentalHousePhone): SettingsData
    {
        $this->pdfDepartmentalHousePhone = $pdfDepartmentalHousePhone;

        return $this;
    }

    public function getPdfCSS(): ?File
    {
        return $this->pdfCSS;
    }

    public function setPdfCSS(?File $pdfCSS): SettingsData
    {
        if ($pdfCSS) {
            $this->pdfCSS = $pdfCSS;
        }

        return $this;
    }

    public function isDocumentaryExchange(): bool
    {
        return $this->documentaryExchange;
    }

    public function setDocumentaryExchange(bool $documentaryExchange): void
    {
        $this->documentaryExchange = $documentaryExchange;
    }

    public function isSendMailDocumentaryExchange(): bool
    {
        return $this->sendMailDocumentaryExchange;
    }

    public function setSendMailDocumentaryExchange(bool $sendMailDocumentaryExchange): void
    {
        $this->sendMailDocumentaryExchange = $sendMailDocumentaryExchange;
    }

    public function getAnswerCallDocument(): string
    {
        return $this->answerCallDocument;
    }

    public function setAnswerCallDocument(string $answerCallDocument): void
    {
        $this->answerCallDocument = $answerCallDocument;
    }

    public function getServiceMessage(): string
    {
        return $this->serviceMessage;
    }

    public function setServiceMessage(string $serviceMessage): void
    {
        $this->serviceMessage = $serviceMessage;
    }

    public function hasInstructorFilter(): bool
    {
        return $this->instructorFilter;
    }

    public function setInstructorFilter(bool $instructorFilter): void
    {
        $this->instructorFilter = $instructorFilter;
    }

    /**
     * @throws ReflectionException
     */
    public function toEntities(): array
    {
        $settingsEntities = [];
        $reader = new AnnotationReader();
        $reflect = new \ReflectionClass($this);
        $objectProps = $reflect->getProperties();

        foreach ($objectProps as $objectProp) {
            $propName = $objectProp->getName();
            $propValue = $this->$propName;
            $reflProp = new \ReflectionProperty($this, $propName);
            $propertyAnnotations = $reader->getPropertyAnnotations($reflProp);

            if (!self::hasObjectArray($propertyAnnotations, DontSave::class)) {
                $setting = new Settings();
                $setting->setName($propName);

                if (self::hasObjectArray($propertyAnnotations, ImageSetting::class)) {
                    if ($propValue instanceof UploadedFile) {
                        $file = $this->uploaderHelper->uploadFile($propValue, UploaderHelper::SETTINGS_IMAGE);
                        $filePath = $this->uploaderHelper->getRelativePath($file);

                        $this->$propName = $file;
                        $setting->setValue($filePath);
                    }
                } elseif (self::hasObjectArray($propertyAnnotations, FileSetting::class)) {
                    if ($propValue instanceof UploadedFile) {
                        $file = $this->uploaderHelper->uploadFile($propValue, UploaderHelper::SETTINGS_FILE);
                        $filePath = $this->uploaderHelper->getRelativePath($file);

                        $this->$propName = $file;
                        $setting->setValue($filePath);
                    }
                } elseif (self::hasObjectArray($propertyAnnotations, BooleanSetting::class)) {
                    $setting->setValue($propValue ? 'true' : 'false');
                } else {
                    $setting->setValue($propValue);
                }

                $settingsEntities[] = $setting;
            }
        }

        return $settingsEntities;
    }

    /**
     * @return $this
     *
     * @throws ReflectionException
     * @throws Exception
     */
    public function initFromDatabase(EntityManagerInterface $entityManager): static
    {
        // On test si la table des paramètres a bien été créée
        // (évite une erreur lors d'un cache:clear avec la BD non initialisée)
        $schemaManager = $entityManager->getConnection()->createSchemaManager();
        $settingsTableName = $entityManager->getClassMetadata(Settings::class)->getTableName();

        if (false === $schemaManager->tableExists($settingsTableName)) {
            return $this;
        }

        $reader = new AnnotationReader();
        $reflect = new \ReflectionClass($this);
        $objectProps = $reflect->getProperties();

        foreach ($objectProps as $objectProp) {
            $propName = $objectProp->getName();
            $reflProp = new \ReflectionProperty($this, $propName);
            $propertyAnnotations = $reader->getPropertyAnnotations($reflProp);

            if (!self::hasObjectArray($propertyAnnotations, DontSave::class)) {
                $value = $this->settingService->getValue($objectProp->getName());

                if (!$value) {
                    continue;
                }

                if (self::hasObjectArray($propertyAnnotations, ImageSetting::class)
                    || self::hasObjectArray($propertyAnnotations, FileSetting::class)
                ) {
                    if (null !== $this->uploaderHelper) {
                        $this->$propName = new File($this->uploaderHelper->getServerPath($value));
                    }
                } elseif (self::hasObjectArray($propertyAnnotations, BooleanSetting::class)) {
                    $this->$propName = filter_var($value, FILTER_VALIDATE_BOOLEAN);
                } else {
                    $this->$propName = $value;
                }
            }
        }

        return $this;
    }
}
