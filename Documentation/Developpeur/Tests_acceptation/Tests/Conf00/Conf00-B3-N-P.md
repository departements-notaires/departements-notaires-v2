# Test d'acceptation `Conf00-B3-N-P`

## Pré-requis

L'application doit être dans la [configuration 00](../Configurations.md#Conf00)

## Test

1. Se connecter en tant qu'étude 3
2. Aller sur l'onglet recherche et saisir les valeurs suivantes :
    * Date de décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date de l'acte de décès : 10/02/2020
    * Prénom : Patricia
    * Nom d'usage : PATATE
    * Date de naissance : 01/01/1911
3. La page résultat de recherche contient : Cette personne est connue de nos services, un message électronique vous est
   envoyé avec les informations relatives à votre demande.
4. L'Affichage dans le tableau mes précédentes recherches contient : aide récupérable
5. Template mail généré est : search_found.html.twig
6. Le courriel reçu par le Notaire contient : est connue du département
7. Le courriel reçu par le Notaire contient :
    * 1 aide récupérable
    * 2 aides non récupérables
8. Le courriel reçu par le Notaire ne contient pas "0 aide en cours d'instruction"
9. Template PDF généré est : search_found_rec.html.twig
10. Le PDF (en pièce jointe du courriel) contient :
    * 1 aide récupérable
    * 2 aides non récupérables
11. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide en cours d'instruction"
