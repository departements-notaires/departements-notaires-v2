<?php

namespace App\Tests\UnitTests\Controller;

use App\Data\SettingsData;
use App\Entity\Settings;
use App\Helper\SearchHelper;
use App\Service\SettingService;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\ORM\EntityManager;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class AdminControllerTest extends KernelTestCase
{
    protected EntityManager $entityManager;

    protected SettingService $settingService;

    public function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->entityManager = $kernel->getContainer()
            ->get('doctrine')
            ->getManager();
        $container = static::getContainer();
        $this->settingService = $container->get('App\Service\SettingService');

        // On purge la base de données
        $purger = new ORMPurger($this->entityManager);
        $purger->purge();
    }

    /**
     * Test de l'enregistrements des paramètres
     */
    public function testSaveSettings()
    {
        $values = [
            'appName'                   => 'D&N',
            'appUrl'                    => 'https://deptnot.test',
            'departmentName'            => 'Nom département',
            'departmentSite'            => 'https://site-departement.test',
            'departmentCilSite'         => 'https://site-cil-departement.test',
            'soundexSearch'             => true,
            'connectionAttempts'        => 5,
            'searchByName'              => SearchHelper::SEARCH_BY_CIVIL_NAME,
            'mailSignature'             => "Signature de mail",
            'mailIntroduction'          => "Introduction du mail",
            'mailNotaBene'              => "Nota bene mail",
            'claims'                    => true,
            'claimsRecoverableStep'     => 0,
            'claimsNonRecoverableStep'  => 0,
            'claimsMail'                => "succession@notaire.test",
            'claimsMailManagement'      => "succession@notaire.test",
            'mailBusinessSignature'     => "Signature de mail métier",
            'pdfDestinationCity'        => 'Dijon',
            'pdfSuccessionMail'         => "succession@notaire.test",
            'pdfDepartmentalHouseName'  => "Maison départementale",
            'pdfDepartmentalHousePhone' => '03 80 65 89 78',
        ];

        $settingsData = new SettingsData($this->settingService);
        $settingsData->setAppName($values['appName']);
        $settingsData->setAppUrl($values['appUrl']);
        $settingsData->setDepartmentName($values['departmentName']);
        $settingsData->setDepartmentSite($values['departmentSite']);
        $settingsData->setDepartmentCilSite($values['departmentCilSite']);
        $settingsData->setSoundexSearch($values['soundexSearch']);
        $settingsData->setConnectionAttempts($values['connectionAttempts']);
        $settingsData->setSearchByName($values['searchByName']);
        $settingsData->setMailSignature($values['mailSignature']);
        $settingsData->setMailIntroduction($values['mailIntroduction']);
        $settingsData->setMailNotaBene($values['mailNotaBene']);
        $settingsData->setClaims($values['claims']);
        $settingsData->setClaimsRecoverableStep($values['claimsRecoverableStep']);
        $settingsData->setClaimsNonRecoverableStep($values['claimsNonRecoverableStep']);
        $settingsData->setClaimsMail($values['claimsMail']);
        $settingsData->setClaimsMailManagement($values['claimsMailManagement']);
        $settingsData->setMailBusinessSignature($values['mailBusinessSignature']);
        $settingsData->setPdfDestinationCity($values['pdfDestinationCity']);
        $settingsData->setPdfSuccessionMail($values['pdfSuccessionMail']);
        $settingsData->setPdfDepartmentalHouseName($values['pdfDepartmentalHouseName']);
        $settingsData->setPdfDepartmentalHousePhone($values['pdfDepartmentalHousePhone']);

        $settingsEntities = $settingsData->toEntities();

        foreach ($settingsEntities as $settingsEntity) {
            $this->entityManager->persist($settingsEntity);
        }

        $this->entityManager->flush();

        $settingsRepository = $this->entityManager->getRepository(Settings::class);
        /** @var Settings $appName */
        $appName = $settingsRepository->findOneBy(['name' => 'appName']);
        $appUrl = $settingsRepository->findOneBy(['name' => 'appUrl']);
        $departmentName = $settingsRepository->findOneBy(['name' => 'departmentName']);
        $departmentSite = $settingsRepository->findOneBy(['name' => 'departmentSite']);
        $departmentCilSite = $settingsRepository->findOneBy(['name' => 'departmentCilSite']);
        $soundexSearch = $settingsRepository->findOneBy(['name' => 'soundexSearch']);
        $connectionAttempts = $settingsRepository->findOneBy(['name' => 'connectionAttempts']);
        $searchByName = $settingsRepository->findOneBy(['name' => 'searchByName']);
        $mailSignature = $settingsRepository->findOneBy(['name' => 'mailSignature']);
        $mailIntroduction = $settingsRepository->findOneBy(['name' => 'mailIntroduction']);
        $mailNotaBene = $settingsRepository->findOneBy(['name' => 'mailNotaBene']);
        $claims = $settingsRepository->findOneBy(['name' => 'claims']);
        $claimsRecoverableStep = $settingsRepository->findOneBy(['name' => 'claimsRecoverableStep']);
        $claimsNonRecoverableStep = $settingsRepository->findOneBy(['name' => 'claimsNonRecoverableStep']);
        $claimsMail = $settingsRepository->findOneBy(['name' => 'claimsMail']);
        $claimsMailManagement = $settingsRepository->findOneBy(['name' => 'claimsMailManagement']);
        $mailBusinessSignature = $settingsRepository->findOneBy(['name' => 'mailBusinessSignature']);
        $pdfDestinationCity = $settingsRepository->findOneBy(['name' => 'pdfDestinationCity']);
        $pdfSuccessionMail = $settingsRepository->findOneBy(['name' => 'pdfSuccessionMail']);
        $pdfDepartmentalHouseName = $settingsRepository->findOneBy(['name' => 'pdfDepartmentalHouseName']);
        $pdfDepartmentalHousePhone = $settingsRepository->findOneBy(['name' => 'pdfDepartmentalHousePhone']);

        $this->assertSame($values['appName'], $appName->getValue());
        $this->assertSame($values['appUrl'], $appUrl->getValue());
        $this->assertSame($values['departmentName'], $departmentName->getValue());
        $this->assertSame($values['departmentSite'], $departmentSite->getValue());
        $this->assertSame($values['departmentCilSite'], $departmentCilSite->getValue());
        $this->assertSame($values['soundexSearch'], boolval($soundexSearch->getValue()));
        $this->assertSame($values['connectionAttempts'], intval($connectionAttempts->getValue()));
        $this->assertSame($values['searchByName'], intval($searchByName->getValue()));
        $this->assertSame($values['mailSignature'], $mailSignature->getValue());
        $this->assertSame($values['mailIntroduction'], $mailIntroduction->getValue());
        $this->assertSame($values['mailNotaBene'], $mailNotaBene->getValue());
        $this->assertSame($values['claims'], boolval($claims->getValue()));
        $this->assertSame($values['claimsRecoverableStep'], intval($claimsRecoverableStep->getValue()));
        $this->assertSame($values['claimsNonRecoverableStep'], intval($claimsNonRecoverableStep->getValue()));
        $this->assertSame($values['claimsMail'], $claimsMail->getValue());
        $this->assertSame($values['claimsMailManagement'], $claimsMailManagement->getValue());
        $this->assertSame($values['mailBusinessSignature'], $mailBusinessSignature->getValue());
        $this->assertSame($values['pdfDestinationCity'], $pdfDestinationCity->getValue());
        $this->assertSame($values['pdfSuccessionMail'], $pdfSuccessionMail->getValue());
        $this->assertSame($values['pdfDepartmentalHouseName'], $pdfDepartmentalHouseName->getValue());
        $this->assertSame($values['pdfDepartmentalHousePhone'], $pdfDepartmentalHousePhone->getValue());
    }

    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
    }
}
