<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Platforms\MariaDBPlatform;
use Doctrine\DBAL\Platforms\MySQLPlatform;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200618122432 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
         $this->abortIf(
             !($this->connection->getDatabasePlatform() instanceof MySQLPlatform) &&
             !($this->connection->getDatabasePlatform() instanceof MariaDbPlatform),
             'Migration can only be executed safely on \'mysql\'.'
         );

        $this->addSql('ALTER TABLE individu ADD date_deces DATE DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
         $this->abortIf(
             !($this->connection->getDatabasePlatform() instanceof MySQLPlatform) &&
             !($this->connection->getDatabasePlatform() instanceof MariaDbPlatform),
             'Migration can only be executed safely on \'mysql\'.'
         );

        $this->addSql('ALTER TABLE individu DROP date_deces');
    }
}
