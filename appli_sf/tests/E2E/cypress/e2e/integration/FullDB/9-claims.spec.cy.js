describe('Créances', () => {
    if (Cypress.env('ENVIRONMENT') === 'dev') {
        before(() => {
            // On rempli la base de données
            cy.exec("cd /srv && docker exec -i depnot-php-1 /entrypoint ./bin/console doctrine:fixture:load --no-interaction --group=use_name --group=claims")
        })
    }

    beforeEach(function () {
        cy.fixture('user-admin')
            .then((user) => {
                this.userAdmin = user
            })
        cy.fixture('user-notaire')
            .then((user) => {
                this.userNotaire = user
            })
        cy.deleteAllMails()
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password, 'admin')
    })

    it('Définition du nom de l\'application et activation des créances', () => {
        cy.visit('/admin/params')
        cy.get('input#settings_appName').type('D&N')
        cy.get('input#settings_claims').type(1)
        cy.get('form[name=settings]').submit()

        cy.contains('Les paramètres ont été modifiés.')
    })

    /**
     * 8.1. Communication de la créance
     * https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/173
     */

    it('Login', function () {
        cy.login(this.userNotaire.username, this.userNotaire.password, 'notaire')
    })

    // 8.1-02 cas CONNU sans date de décès renseignée sur la table individus
    // https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/191
    it('Personne connue, sans date de décès', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2022',
            deathLocation:        'LILLE',
            deathCertificateDate: '12/02/2022',
            firstName:            'Guy',
            useName:              'VANECKE',
            birthDate:            '02/03/1949'
        })

        cy.contains('Cette personne est connue de nos services')
        cy.checkMailContent(this.userNotaire.email, "<strong>connue<=\\r\\n/strong>").should('be.true')
        cy.checkMailContent(this.userNotaire.email, "renouveler votre demande apr=C3=A8s un d=C3=A9lai de 72 he=\\r\\nures").should('be.true')
        cy.checkMailContent(this.userNotaire.email, "d=E2=80=99int=C3=A9grer l=E2=80=99info=\\r\\nrmation de la date\\r\\n            de d=C3=A9c=C3=A8s").should('be.true')
    })

    // 8.1-03 cas CONNU avec date de décès renseignée sur la table individus différente de la date de décès saisie par le Notaire
    // https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/192
    it('Personne connue, date de décès différentes entre la base de données et la recherche', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2022',
            deathLocation:        'LILLE',
            deathCertificateDate: '12/02/2022',
            firstName:            'Michèle',
            useName:              'VANDRINE',
            birthDate:            '19/04/1952'
        })

        cy.contains('Cette personne est connue de nos services')
        cy.checkMailContent(this.userNotaire.email, "<strong>connue</strong>").should('be.true')
        cy.checkMailContent(this.userNotaire.email, "La date de d=C3=A9c=C3=A8s semble incorrecte").should('be.true')
    })

    // 8.1-04 cas Ambigü avec date de naissance renseignée sur la table individus différente de la date de naissance saisie par le Notaire
    // https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/193
    it('Personne ambigüe, dates de naissance différentes (j/m) entre la base de données et la recherche', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '30/12/2009',
            deathLocation:        'LILLE',
            deathCertificateDate: '30/12/2009',
            firstName:            'Michèle',
            useName:              'VANDRINE',
            birthDate:            '05/12/1952'
        })

        cy.contains('Les éléments que vous avez saisis ne permettent pas d\'établir avec certitude l\'identité')
        cy.checkMailContent(this.userNotaire.email, "Si vous pensez avoir commis une erreur de saisie").should('be.true')
        cy.checkMailContent(this.userNotaire.email, "Dans le cas contraire, je vous invite =\\r\\n=C3=A0 confirmer que les renseignements").should('be.true')
    })

    // 8.1-07 Créance récupérable affichable avec montant compris entre les seuils paramétrés (bornes incluses) ou pas de créance récupérable
    // https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/195
    // 8.1-11 : Créance non récupérable affichable avec montant compris entre les seuils paramétrés (bornes incluses)
     // ou créance non récupérable non affichable ou pas de créance non récupérable
    // https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/199
    it('Personne connue, créance récupérable affichable avec montant =< seuil', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '30/12/2009',
            deathLocation:        'LILLE',
            deathCertificateDate: '30/12/2009',
            firstName:            'Michèle',
            useName:              'VANDRINE',
            birthDate:            '19/04/1952'
        })

        cy.contains('Cette personne est connue de nos services')
        cy.checkMailContent(this.userNotaire.email, "<strong>connue</strong>").should('be.true')
        cy.checkMailContent(this.userNotaire.email, "la succession n=\\r\\n=E2=80=99est redevable d=E2=80=99aucun indu").should('be.true')
    })
    //
    // // 8.1-08 Créance récupérable non affichable avec montant en dehors des seuils paramétrés (strictement)
    // // https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/196
    it('Personne connue, créance récupérable non affichable avec montant > seuil', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '20/11/2015',
            deathLocation:        'LILLE',
            deathCertificateDate: '20/11/2015',
            firstName:            'Patrick',
            useName:              'LIMIER',
            birthDate:            '26/03/1957'
        })

        cy.contains('Cette personne est connue de nos services')
        cy.checkMailContent(this.userNotaire.email, "<strong>conn=\\r\\nue</strong>").should('be.true')
        cy.checkMailContent(this.userNotaire.email, "la succession n=E2=80=99est redevable d=E2=80=99aucun indu").should('be.true')
        cy.checkMailContent(this.userNotaire.email, "consolider le montant de l\\'indu").should('be.true')
        cy.checkMailContent(this.userNotaire.email, "Le service=\\r\\n charg=C3=A9 du recouvrement des cr=C3=A9ances reviendra vers vous dans les=\\r\\n\\r\\n                                            meilleurs d=C3=A9lais pour v=\\r\\nous").should('be.true')
    })
    //
    // // 8.1-10 Créance non récupérable affichable avec montant en dehors des seuils paramétrés (strictement)
    // // https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/198
    it('Personne connue, créance non récupérable affichable avec montant > seuil', function () {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '09/08/2020',
            deathLocation:        'LILLE',
            deathCertificateDate: '09/08/2020',
            firstName:            'Francis',
            useName:              'HAUBRY',
            birthDate:            '04/04/1947'
        })

        cy.contains('Cette personne est connue de nos services')
        cy.checkMailContent(this.userNotaire.email, "<strong>conn=\\r\\nue</strong>").should('be.true')
        cy.checkMailContent(this.userNotaire.email, "le montant calcul=C3=A9 au jour =\\r\\nde votre connexion").should('be.true')
        cy.checkMailContent(this.userNotaire.email, "l=\\r\\na succession n=E2=80=99est redevable d=E2=80=99aucun indu").should('be.true')
    })

    it('Déconnexion', () => {
        cy.logout()
    })
})
