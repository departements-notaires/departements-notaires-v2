<?php

namespace App\Helper;

class FixturesHelper
{
    public const GROUP_USE_NAME = 'use_name';
    public const GROUP_CIVIL_NAME = 'civil_name';
    public const GROUP_USERS = 'users';
    public const GROUP_WITHOUT_PERSONS = 'without_persons';
}
