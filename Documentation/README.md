# Documentation

## Documentation fonctionnelle

La documentation fonctionnelle, à destination des utilisateur·ices, est maintenant disponible sur le site dédié
**[departements-et-notaires.adullact.org](https://departements-et-notaires.adullact.org/)**.
Ce site propose notamment une documentation à destination des notaires, des agent·es du département
(instructeur·ice ou administrateur·ices), et des opérateurs pour l'installation et la maintenance.

Le code correspondant est hébergé sur le GitLab de l'ADULLACT,
[gitlab.adullact.net/departements-notaires/departements-et-notaires.adullact.org](https://gitlab.adullact.net/departements-notaires/departements-et-notaires.adullact.org)
, la documentation fonctionnelle doit y être maintenue à jour.

## Documentation technique

La documentation technique, à destination des contributeur·ices du code source, reste contenue dans le présent dépôt :

* [Documentation pour les développeur·euses](Developpeur)
