# Tests d'acceptation

## Création des fichiers de test

Les 4680 fichiers ont été créés avec la commande suivante :

```shell
for i in $(seq -f %02g 0 19); do
  for j in B1 B2 B3 B4 C1 C2 D1 D2 E1 E2 F1 F2 F3 F4 F5 F6 F7 F8 G1 G2 G3 G4 H1 H2 H3 H4 ; do
    for k in N NCT NCST; do
      for l in P PCT PCST; do
        echo "Conf${i}-${j}-${k}-${l}.md"
        mkdir -p "Conf${i}"
        touch "Conf${i}/Conf${i}-${j}-${k}-${l}.md"
      done
    done
  done
done
```
