# Créer une version de Départements & Notaires

## Prérequis

* [ ] Les entrées du `composer.json` sont à jour sur la dernière version (objectif : ne pas utiliser une lib avec un
  trou de sécurité)
* [ ] La branche de travail est rebasée sur `master` (i.e. elle commence à partir du dernier commit de `master`)
* [ ] La CI est verte (i.e. tous les tests sont déroulés avec succès)
* [ ] Le numéro de la version à créer est saisi dans `appli_sf/composer.json`, champ `version`
* [ ] La doc
  [Documentation > Opérateur > Mise à jour](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Operateur/mise-a-jour.md)
  est à jour, décrit clairement les éventuels *breaking changes* et explique comment les traiter
* [ ] Le CHANGELOG est à jour, listant les modifications / ajouts / corrections avec les liens vers les tickets
  correspondants

## Tâches effectuées par l'ADULLACT

* [ ] Fusionner la branche dans `master` (rappel : la CI de la branche étant verte)
* [ ] Ajouter le tag de la version sur le commit de merge (sur master donc)

## Publication du package de la version

**Note** : depuis la version 2.5.1 (2023-11-18), l'artefact de compilation est automatiquement téléversé sur le dépôt.

* [ ] Récupérer manuellement l'artefact de build (fichier tar.gz) du tag sur la branche master
  (rappel : dont la CI doit être verte)
* [ ] Envoyer ce fichier dans
  le [Packages Registry](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/packages).

Pour publier dans le *Package registry* :

* Disposer d'un jeton d'accès privé à générer dans
  les [paramètres du projet dans Gitlab](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2)
  Plus d'informations dans la [doc Gitlab *Personal Access
  Token*](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
* Envoyer le fichier par une requête PUT.
  Cf [Doc Gitlab Package Registry](https://docs.gitlab.com/ee/user/packages/generic_packages/#gitlab-generic-packages-repository)

Exemple de requête :

```shell
curl --header "PRIVATE-TOKEN: monToken" \
 --upload-file departements-notaires-X.Y.Z.tgz \
 "https://gitlab.adullact.net/api/v4/projects/605/packages/generic/Departements_et_Notaires/X.Y.Z/departements-notaires-X.Y.Z.tgz"
```

Avec :

* `605` étant l'id du dépôt Départements & Notaires v2 du gitLab d'Adullact
* `X.Y.Z` étant le numéro de version conforme à [Semver](https://semver.org/)

## Déploiement

- [ ] Déployer sur la version le [serveur de recette](https://depnot.ovh.adullact.org/)
- [ ] Importer
  le [jeu de données de test](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/imports/donnees_test.sql)
