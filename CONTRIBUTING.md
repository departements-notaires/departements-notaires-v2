# Contribuer à Départements & Notaires

Vous voulez contribuer à Départements & Notaires, bravo ! Et merci :)

Voici les points méthodologiques et techniques importants

## 1. Au début était le ticket

Avant toute contribution, merci de créer un ticket (*issue*) dans Gitlab.

S'il s'agit d'un bug, merci de veiller à bien décrire les étapes pour reproduire le bug. Cette partie est très
importante pour permettre une revue par les pairs.

Si le ticket est lié de près ou de loin à la recherche d'individu, merci de prendre le temps de choisir un individu type
dans l'un des jeux de données de test ([données de test classique](appli_sf/imports/donnees_test.sql)
ou [données de test pour instructeurs de territoire](appli_sf/imports/donnees_test_instructeurs_territoire.sql)). Ce
point est particulièrement important car c'est lui qui permet de nous assurer du caractère reproductible d'une
situation, indépendamment du jeu de données réelles que peut avoir un département ou un autre.

Si jamais il manquait un individu dans l'un des jeux de données, il vous est possible d'en ajouter. Merci de bien
chercher qu'un tel individu n'existe pas déjà, il en existe (à fin 2023) plus de 900 !

Une fois le ticket créé, informez l'ADULLACT. Une phase d'échange s'amorce pour éventuellement préciser le ticket.

## 2. Puis vint la *merge request* (MR)

Créez une MR. Les attendus sont les suivants :

- le code
- les tests associés au code
- la documentation (rangée dans le [dossier `Documentation`](Documentation)).

Une fois la MR créé, informez l'ADULLACT.

## 3. Revue

La MR est passée en revue. Une phase d'échange s'amorce pour éventuellement préciser la MR.

## 4. Fusion

La branche est fusionnée dans le code source.

