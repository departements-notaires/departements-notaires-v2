describe('Recherche avec table individu vide', () => {
    if (Cypress.env('ENVIRONMENT') === 'dev') {
        before(() => {
            // On rempli la base de données (sans individu)
            cy.exec("cd /srv && docker exec -i depnot-php-1 /entrypoint ./bin/console doctrine:fixture:load --no-interaction --group=without_persons")
        })
    }

    beforeEach(function () {
        cy.fixture('user-notaire')
            .then((user) => {
                this.userNotaire = user
            })
    })
    it('Login', function () {
        cy.login(this.userNotaire.username, this.userNotaire.password, 'notaire')
    })

    it('Personne connue, aide récupérable', () => {
        cy.visit('/search')
        cy.fillSearch({
            deathDate:            '10/02/2020',
            deathLocation:        'VILLEFRANCHE',
            deathCertificateDate: '12/02/2020',
            firstName:            'Aimé',
            useName:              'OLMO',
            birthDate:            '06/12/1977'
        })

        cy.contains('Cette personne est inconnue de nos services')
    })

    it('Déconnexion', () => {
        cy.logout()
    })
})
