<?php

namespace App\Controller;

use App\Helper\GraphHelper;
use App\Helper\SearchHelper;
use App\Helper\StatsHelper;
use App\Repository\SearchLogRepository;
use App\Repository\UserLogRepository;
use App\Repository\UserRepository;
use App\Service\PdfService;
use App\Service\SettingsDataService;
use App\Service\ThemeService;
use DateTime;
use Exception;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

class StatsController extends BaseController
{
    /**
     * UserController constructor.
     */
    public function __construct(
        SettingsDataService $settingsDataService,
        protected TranslatorInterface $translator,
        ThemeService $themeService,
        protected UserRepository $userRepository,
        protected UserLogRepository $userLogRepository,
        protected SearchLogRepository $searchLogRepository
    ) {
        parent::__construct($settingsDataService, $themeService);
    }

    /**
     * Affichage du tableau des statistiques.
     *
     * @throws Exception
     */
    #[Route('/stats', name: 'stats')]
    public function statsAction(Request $request): Response
    {
        $dateTo = new DateTime();
        $dateTo->setTime(23, 59, 59, 999999);
        $dateFrom = clone $dateTo;
        $dateFrom
            ->modify('first day of -4 month') // affiche au maximum 5 mois
            ->setTime(0, 0); // force l'heure à 00:00

        [$stats, $tableHeader] = $this->getStats($dateFrom, $dateTo);

        $displayedStats = StatsHelper::verticalToHorizontalStatsArray($stats, $this->translator);

        // Création formulaire de téléchargement du pdf à partir d'une date de début et une date de fin
        $form = $this->createFormBuilder()
            ->add(
                'startDate',
                DateType::class,
                [
                    'label' => 'searchLogsFilter.startDate',
                    'required' => true,
                    'widget' => 'single_text',
                    'data' => $dateFrom,
                ]
            )->add(
                'endDate',
                DateType::class,
                [
                    'label' => 'searchLogsFilter.endDate',
                    'required' => true,
                    'widget' => 'single_text',
                    'data' => $dateTo,
                ]
            )->add(
                'send',
                SubmitType::class,
                [
                    'label' => 'stats.sendPdf',
                ]
            )->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            /** @var DateTime $startDate */
            $startDate = $data['startDate'];
            /** @var DateTime $startDate */
            $endDate = $data['endDate'];

            return $this->redirectToRoute(
                'stats_pdf',
                [
                    'date_from' => $startDate->format('Y-m-d'),
                    'date_to' => $endDate->format('Y-m-d'),
                ]
            );
        }

        return $this->render(
            'stats/stats.html.twig',
            [
                'tableHeader' => $tableHeader,
                'stats' => $displayedStats,
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo,
                'statsForm' => $form->createView(),
            ]
        );
    }

    /**
     * Affichage du graph.
     *
     * @throws Exception
     */
    #[Route('/stats/graph', name: 'stats_graph')]
    public function graphAction(Request $request): Response
    {
        $dateFrom = DateTime::createFromFormat('Y-m-d', $request->query->get('date_from'));
        $dateTo = DateTime::createFromFormat('Y-m-d', $request->query->get('date_to'));

        [$stats] = $this->getStats($dateFrom, $dateTo);

        $graph = new GraphHelper($this->translator);

        $graph->render($dateTo, $dateFrom, $stats, false);

        return new Response(
            'Graph render is done',
            Response::HTTP_OK
        );
    }

    /**
     * Affichage des stats dans un PDF.
     *
     * @throws Html2PdfException
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    #[Route('/stats/pdf', name: 'stats_pdf')]
    public function showPdfAction(Request $request, PdfService $pdfService): RedirectResponse
    {
        $dateFrom = DateTime::createFromFormat('Y-m-d', $request->query->get('date_from'));
        $dateTo = DateTime::createFromFormat('Y-m-d', $request->query->get('date_to'));

        [$stats, $tableHeader] = $this->getStats($dateFrom, $dateTo);
        $displayedStats = StatsHelper::verticalToHorizontalStatsArray($stats, $this->translator);

        $graph = new GraphHelper($this->translator);
        $graphFile = $graph->render($dateTo, $dateFrom, $stats, true);

        $pdfContent = $this->renderView(
            'stats/pdf.html.twig',
            [
                'dateFrom' => $dateFrom,
                'dateTo' => $dateTo,
                'tableHeader' => $tableHeader,
                'stats' => $displayedStats,
                'graphFile' => $graphFile,
            ]
        );

        $fileName = 'notaires-statistiques_'.$dateFrom->format('Y-m').'_'.$dateTo->format('Y-m').'.pdf';
        $pdfService->create();
        $pdfService->generatePdf($pdfContent, $fileName, 'D');

        return $this->redirect('stats');
    }

    /**
     * Récupère les stats du site.
     *
     * @throws Exception
     */
    private function getStats(
        DateTime $dateFrom,
        DateTime $dateTo,
    ): array {
        $nbResponsesByType = [
            SearchHelper::SEARCH_STATUS_FOUND_REC => 0,
            SearchHelper::SEARCH_STATUS_FOUND_NOTREC => 0,
            SearchHelper::SEARCH_STATUS_HAS_REQUEST => 0,
            SearchHelper::SEARCH_STATUS_AMBIGUOUS => 0,
            SearchHelper::SEARCH_STATUS_NOTFOUND => 0,
        ];

        $stats = [];
        $monthsList = [];
        $iDate = clone $dateTo;

        // Initialisation du tableau de stats
        while ($iDate >= $dateFrom) {
            $monthsList[] = $iDate->format('U');
            $stats[$iDate->format('Y-m')] = [
                'nbRequests' => 0,
                'nbResponses' => 0,
                'nbResponsesByType' => $nbResponsesByType,
                'nbConnections' => 0,
                'nbUniqueUsers' => 0,
            ];

            $iDate->modify('last day of previous month');
        }

        $searchStatsResult = $this->searchLogRepository->findSearchStats($nbResponsesByType);
        $searchNumberResult = $this->searchLogRepository->findNumberSearchStats();

        // Remplissage du tableau de stats avec les données de recherche
        foreach ($searchStatsResult as $month => $responses) {
            if (!array_key_exists($month, $stats)) {
                continue;
            }

            $curStats = &$stats[$month];

            $curStats['nbResponses'] = array_sum($responses);
            $curStats['nbRequests'] = $searchNumberResult[$month];
            $curStats['nbResponsesByType'] = array_replace($curStats['nbResponsesByType'], $responses);
        }

        // Nombre de connexions pour chaque mois
        $countLogResult = $this->userLogRepository->countLogByMonth($dateFrom, $dateTo);

        foreach ($countLogResult as $result) {
            if (!array_key_exists($result['month'], $stats)) {
                continue;
            }

            $curStats = &$stats[$result['month']];
            $curStats['nbConnections'] = $result['nb'];
        }

        // Nombre de connexions unique pour chaque mois
        $countUniqLogResult = $this->userLogRepository->countUniqLogByMonth($dateFrom, $dateTo);

        foreach ($countUniqLogResult as $result) {
            if (!array_key_exists($result['month'], $stats)) {
                continue;
            }

            $curStats = &$stats[$result['month']];
            $curStats['nbUniqueUsers'] = $result['nb'];
        }

        return [$stats, $monthsList];
    }
}
