# Départements & Notaires v2

![](Documentation/Images/Logo-Departements-Notaires.jpg)

**Départements et Notaires** est un extranet permettant d'apporter une réponse en temps réel aux études notariales
chargées d’une succession s'interrogeant sur l'existence éventuelle d'une créance du Département au titre de l'aide
sociale.

**Départements et Notaires** a été initialement conçu et développé par le Département du Rhône en lien avec la Chambre
des Notaires du Rhône, et déployé la première fois en janvier 2016 sous le nom **Rhône + Notaires**.

## Fonctionnalités

* **Pour les notaires** :
    * Recherche et réponse immédiate, à l'écran et confirmation par email avec lettre jointe PDF
    * Historique de leurs propres recherches
* **Pour les gestionnaires** :
    * Historiques de toutes les recherches réalisées
    * Statistiques

## Documentation fonctionnelle

La documentation fonctionnelle, à destination des utilisateur·ices, est maintenant disponible sur le site dédié
**[departements-et-notaires.adullact.org](https://departements-et-notaires.adullact.org/)**.
Ce site propose notamment une documentation à destination des notaires, des agent·es du département
(instructeur·ice ou administrateur·ices), et des opérateurs pour l'installation et la maintenance.

Le code correspondant est hébergé sur le GitLab de l'ADULLACT,
[gitlab.adullact.net/departements-notaires/departements-et-notaires.adullact.org](https://gitlab.adullact.net/departements-notaires/departements-et-notaires.adullact.org)
, la documentation fonctionnelle doit y être maintenue à jour.

## Documentation technique

La documentation technique, à destination des contributeur·ices du code source, est contenue dans le présent dépôt :

* [Documentation pour les développeur·euses](Developpeur)
* [Fichier CHANGELOG](CHANGELOG.md)
