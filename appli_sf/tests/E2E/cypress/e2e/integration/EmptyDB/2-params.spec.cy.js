describe('Paramètres de l\'application', () => {
    if (Cypress.env('ENVIRONMENT') === 'dev') {
        before(() => {
            // On rempli la base de données (sans individu)
            cy.exec("cd /srv && docker exec -i  depnot-php-1 /entrypoint ./bin/console doctrine:fixture:load --no-interaction --group=without_persons")
        })
    }

    beforeEach(function () {
        cy.fixture('user-admin')
            .then((user) => {
                this.userAdmin = user
            })
    })

    it('Login', function () {
        cy.login(this.userAdmin.username, this.userAdmin.password, 'admin')
    })

    it('Page des paramètres', () => {
        cy.visit('/admin/params')

        cy.location('href').should('match', /admin\/params\/?$/)
        cy.contains('Paramètres de l\'application')
    })

    it('Définir nom de l\'application', () => {
        cy.get('input#settings_appName').type('D&N')
        cy.get('form[name=settings]').submit()

        cy.contains('Les paramètres ont été modifiés.')
    })

    it('Ajouter un logo', () => {
        cy.fixture('images/logo-rhone-blanc.svg').then(fileContent => {
            cy.get('input[type="file"]#settings_appLogo').attachFile({
                fileContent: fileContent.toString(),
                fileName:    'logo-rhone-blanc.svg',
                mimeType:    'image/svg+xml'
            });
        });

        cy.get('form[name=settings]').submit()

        cy.get('input#settings_appLogo').parent().next().should('have.class', 'image-preview')
        cy.get('a.logo-department').children().should('have.length', 1) // le logo s'affiche
    })

    it('Déconnexion', () => {
        cy.logout()
    })
})
