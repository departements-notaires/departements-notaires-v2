<?php

namespace App\Controller;

use App\Data\SettingsData;
use App\Service\SettingsDataService;
use App\Service\ThemeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Twig\Error\LoaderError;

class BaseController extends AbstractController
{
    protected SettingsData $settingsData;

    protected string $appTheme;

    /**
     * BaseController constructor.
     */
    public function __construct(SettingsDataService $settingsDataServiceService, ThemeService $themeService)
    {
        $this->settingsData = $settingsDataServiceService->getData();
        $this->appTheme = $themeService->getTheme();
    }

    /**
     * Réécriture de la fonction renderView pour permettre la surcharge des templates.
     */
    protected function render(string $view, array $parameters = [], ?Response $response = null): Response
    {
        if (!$this->container->has('twig')) {
            throw new \LogicException('You can not use the "render" method if the Twig Bundle is not available. Try running "composer require symfony/twig-bundle".');
        }

        $themeView = $this->appTheme ? $this->appTheme.'/'.$view : $view;

        return parent::render($themeView, $parameters, $response);
    }

    /**
     * Réécriture de la fonction renderView pour permettre la surchage des templates.
     *
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    protected function renderView(string $view, array $parameters = []): string
    {
        if (!$this->container->has('twig')) {
            throw new \LogicException('You can not use the "renderView" method if the Twig Bundle is not available. Try running "composer require symfony/twig-bundle".');
        }

        $themeView = $this->appTheme.'/'.$view;

        try {
            return $this->container->get('twig')->render($themeView, $parameters);
        } catch (LoaderError $e) {
            $view = 'default/'.$view;

            return $this->container->get('twig')->render($view, $parameters);
        }
    }


    /**
     * Retourne le chemin du template en fonction du theme en cours.
     */
    protected function getTemplateWithActifTheme(string $view): string
    {
        return $this->appTheme.'/'.$view;
    }
}
