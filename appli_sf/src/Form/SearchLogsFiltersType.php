<?php

namespace App\Form;

use App\Entity\SearchLog;
use App\Entity\User;
use App\Helper\SearchHelper;
use App\Helper\UserHelper;
use App\Repository\SearchLogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;

class SearchLogsFiltersType extends AbstractType
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var Security
     */
    private $security;

    /**
     * SearchLogsFiltersType constructor.
     */
    public function __construct(EntityManagerInterface $entityManager, Security $security)
    {
        $this->entityManager = $entityManager;
        $this->security = $security;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'responseType',
                ChoiceType::class,
                [
                    'label' => 'searchLogsFilter.responseType',
                    'required' => false,
                    'choices' => SearchHelper::getResponseTypesChoices(),
                    'placeholder' => 'responseType.empty',
                ]
            )->add(
                'name',
                ChoiceType::class,
                [
                    'label' => 'searchLogsFilter.name',
                    'choices' => $this->fillNameChoices(),
                    'required' => false,
                    'placeholder' => 'searchLogsFilter.namePlaceholder',
                ]
            )->add(
                'startDate',
                DateType::class,
                [
                    'label' => 'searchLogsFilter.startDate',
                    'required' => false,
                    'widget' => 'single_text',
                ]
            )->add(
                'endDate',
                DateType::class,
                [
                    'label' => 'searchLogsFilter.endDate',
                    'required' => false,
                    'widget' => 'single_text',
                ]
            )->add(
                'reset',
                ResetType::class,
                [
                    'label' => 'Réinitialiser',
                    'attr' => ['class' => 'border-button'],
                ]
            )->add(
                'send',
                SubmitType::class,
                [
                    'label' => 'searchLogsFilter.send',
                ]
            );

        if ($this->security->isGranted(UserHelper::ROLE_AGENT)) {
            $builder->add(
                'user',
                EntityType::class,
                [
                    'label' => 'searchLogsFilter.user',
                    'required' => false,
                    'class' => User::class,
                    'choice_label' => 'username',
                    'placeholder' => 'searchLogsFilter.userPlaceholder',
                ]
            );
        }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }

    private function fillNameChoices(): array
    {
        /** @var SearchLogRepository $searchLogRepository */
        $searchLogRepository = $this->entityManager->getRepository(SearchLog::class);
        /** @var User $user */
        $user = $this->security->getUser();

        $currentUser = null;

        if (!$this->security->isGranted(UserHelper::ROLE_AGENT)) {
            $currentUser = $user;
        }

        $searchLogs = $searchLogRepository->findNames($currentUser);

        $names = [];

        /** @var SearchLog $searchLog */
        foreach ($searchLogs as $searchLog) {
            if ($searchLog->getUseName()) {
                $names[$searchLog->getUseName()] = $searchLog->getUseName();
            }
            if ($searchLog->getCivilName()) {
                $names[$searchLog->getCivilName()] = $searchLog->getCivilName();
            }
        }

        ksort($names, SORT_NATURAL | SORT_FLAG_CASE);

        return $names;
    }
}
