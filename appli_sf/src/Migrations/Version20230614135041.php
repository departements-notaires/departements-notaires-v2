<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230614135041 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add helplabel for instructeur table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE instructeur ADD help_label LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE instructeur DROP help_label');
    }
}
