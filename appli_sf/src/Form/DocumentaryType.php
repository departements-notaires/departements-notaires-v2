<?php

namespace App\Form;

use App\Entity\SearchLog;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Contracts\Translation\TranslatorInterface;

class DocumentaryType extends AbstractType
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $documentConstraint = [
            new File(
                [
                    'maxSize' => '5M',
                    'mimeTypes' => [
                        'image/png',
                        'image/jpeg',
                        'image/gif',
                        'application/pdf',
                    ],
                    'mimeTypesMessage' => $this->translator->trans('documentaryExchange.error.documentFile'),
                ]
            ),
        ];
        $builder
            ->add(
                'person',
                HiddenType::class
            )
            ->add(
                'id',
                HiddenType::class,
                [
                    'data' => $options['data']->getId(),
                    'mapped' => false,
                ]
            )
            ->add(
                'deathDocument',
                FileType::class,
                [
                    'required' => true,
                    'mapped' => false,
                    'label' => 'documentaryExchange.deathDocument',
                    'allow_file_upload' => '.png, .jpg, .jpeg, .gif, .pdf',
                    'attr' => [
                        'accept' => 'image/png, image/jpeg, image/gif, application/pdf',
                    ],
                    'constraints' => $documentConstraint,
                ]
            )
            ->add(
                'netDocument',
                FileType::class,
                [
                    'required' => false,
                    'mapped' => false,
                    'label' => 'documentaryExchange.netDocument',
                    'allow_file_upload' => '.png, .jpg, .jpeg, .gif, .pdf',
                    'attr' => [
                        'accept' => 'image/png, image/jpeg, image/gif, application/pdf',
                    ],
                    'constraints' => $documentConstraint,
                ]
            )
            ->add(
                'otherDocument',
                FileType::class,
                [
                    'required' => false,
                    'mapped' => false,
                    'label' => 'documentaryExchange.otherDocument',
                    'allow_file_upload' => '.png, .jpg, .jpeg, .gif, .pdf',
                    'attr' => [
                        'accept' => 'image/png, image/jpeg, image/gif, application/pdf',
                    ],
                    'constraints' => $documentConstraint,
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(
            [
                'data_class' => SearchLog::class,
            ]
        );
    }
}
