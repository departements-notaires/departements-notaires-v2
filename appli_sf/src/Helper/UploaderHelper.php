<?php

namespace App\Helper;

use Symfony\Component\Asset\Context\RequestStackContext;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploaderHelper
{
    public const SETTINGS_IMAGE = 'settings/images';
    public const SETTINGS_FILE = 'settings/files';
    public const DOCUMENTARY_EXCHANGE = 'documents';

    /**
     * @var string
     */
    private $uploadsPath;

    private string $privateDir;

    /**
     * @var string
     */
    private $uploadedAssetsBaseUrl;

    /**
     * @var RequestStackContext
     */
    private $requestStackContext;

    /**
     * @var Filesystem
     */
    private $filesystem;

    /** @var string */
    private $encryptionKey;

    /**
     * UploaderHelper constructor.
     */
    public function __construct(
        RequestStackContext $requestStackContext,
        string $uploadsPath,
        string $uploadedAssetsBaseUrl,
        string $privateDir,
        string $encryptionKey
    ) {
        $this->uploadsPath = $uploadsPath;
        $this->uploadedAssetsBaseUrl = $uploadedAssetsBaseUrl;
        $this->requestStackContext = $requestStackContext;
        $this->privateDir = $privateDir;
        $this->filesystem = new Filesystem();
        $this->encryptionKey = $encryptionKey;
    }

    /**
     * Upload d'un fichier.
     */
    public function uploadFile(UploadedFile $uploadedFile, string $fileDir): File
    {
        $destination = $this->uploadsPath.'/'.$fileDir;

        $originalFilename = pathinfo($uploadedFile->getClientOriginalName());
        $safeFilename = \transliterator_transliterate(
            'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()',
            $originalFilename['filename']
        );

        $newFilename = $safeFilename.'-'.uniqid().'.'.$originalFilename['extension'];

        return $uploadedFile->move(
            $destination,
            $newFilename
        );
    }

    /**
     * Upload d'un fichier privée.
     */
    public function uploadPrivateFile(UploadedFile $uploadedFile, string $fileDir): string
    {
        if (!$this->filesystem->exists($this->privateDir)) {
            $this->filesystem->mkdir($this->privateDir, 0755);
        }
        if (!$this->filesystem->exists($this->privateDir.'/'.$fileDir)) {
            $this->filesystem->mkdir($this->privateDir.'/'.$fileDir, 0755);
        }

        $destination = $this->privateDir.'/'.$fileDir;

        $originalFilename = pathinfo($uploadedFile->getClientOriginalName());
        $safeFilename = transliterator_transliterate(
            'Any-Latin; Latin-ASCII; [^A-Za-z0-9_] remove; Lower()',
            $originalFilename['filename']
        );

        $newFilename = $safeFilename.'-'.uniqid().'.'.$originalFilename['extension'];

        $this->encryptFile($uploadedFile->getRealPath(), $destination.'/'.$newFilename, $this->encryptionKey);

        return $destination.'/'.$newFilename;
    }

    /**
     * Retourne le chemin d'accès public à un fichier.
     */
    public function getPublicPath(File $file): string
    {
        $path = $this->getRelativePath($file);

        return '/'.$this->uploadedAssetsBaseUrl.'/'.$path;
    }

    /**
     * Retourne le chemin relatif d'accès à un fichier.
     */
    public function getRelativePath(File $file): string
    {
        $path = $this->filesystem->makePathRelative($file->getPath(), $this->uploadsPath);

        return $path.$file->getFilename();
    }

    /**
     * Retourne le chemin sur le serveur.
     */
    public function getServerPath(string $path): string
    {
        return $this->requestStackContext
                ->getBasePath().$this->uploadsPath.'/'.$path;
    }

    public function encryptFile($source, $dest, $key)
    {
        $cipher = 'aes-256-cbc';
        $ivLenght = openssl_cipher_iv_length($cipher);
        $iv = openssl_random_pseudo_bytes($ivLenght);

        $fpSource = fopen($source, 'rb');
        $fpDest = fopen($dest, 'w');

        fwrite($fpDest, $iv);

        while (!feof($fpSource)) {
            $plaintext = fread($fpSource, $ivLenght * 10000);
            $ciphertext = openssl_encrypt($plaintext, $cipher, $key, OPENSSL_RAW_DATA, $iv);
            $iv = substr($ciphertext, 0, $ivLenght);

            fwrite($fpDest, $ciphertext);
        }

        fclose($fpSource);
        fclose($fpDest);
    }
}
