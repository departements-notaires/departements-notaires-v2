<?php

namespace App\Command;

use App\Entity\SearchLog;
use App\Repository\SearchLogRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

class DocumentaryCleanupCommand extends Command
{
    /** @var Filesystem */
    private $filesystem;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /** @var string */
    private $expirationDocument;

    public function __construct(Filesystem $filesystem, EntityManagerInterface $entityManager, string $expirationDocument)
    {
        parent::__construct();
        $this->filesystem = $filesystem;
        $this->entityManager = $entityManager;
        $this->expirationDocument = $expirationDocument;
    }

    protected function configure()
    {
        $this
            ->setName('app:document:cleanup')
            ->setDescription(' Les documents téléversés par les notaires sont supprimés automatiquement passé un certain délai')
            ->setHelp('Permet de supprimer les documents téléversés.');
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $finder = new Finder();
        $finder->depth('== 0')->depth('< 3');
        $finder->files()->in(realpath(__DIR__.'/../../private/documents'));

        $date = new \DateTime();
        $interval = \DateInterval::createFromDateString($this->expirationDocument);
        $dateToCleanup = strtotime($date->sub($interval)->format('Y-m-d h:i:s'));
        if ($finder->hasResults()) {
            foreach ($finder->files() as $file) {
                if ($file->getCTime() > $dateToCleanup) {
                    /** @var SearchLogRepository $searchLogRepository */
                    $searchLogRepository = $this->entityManager->getRepository(SearchLog::class);
                    $logs = $searchLogRepository->findByDocument($file->getPathname());
                    foreach ($logs as $log) {
                        if ($log->getDeathDocument() === $file->getPathname()) {
                            $log->setDeathDocument(null);
                        }
                        if ($log->getNetDocument() === $file->getPathname()) {
                            $log->setNetDocument(null);
                        }
                        if ($log->getOtherDocument() === $file->getPathname()) {
                            $log->setOtherDocument(null);
                        }
                    }
                    $this->filesystem->remove($file->getPathname());
                    $this->entityManager->flush();
                }
            }
        }

        return Command::SUCCESS;
    }
}
