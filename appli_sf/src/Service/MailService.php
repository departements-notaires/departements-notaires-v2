<?php

namespace App\Service;

use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Exception\RfcComplianceException;
use Twig\Environment;

class MailService
{
    private $mailer;
    private $mailSender;
    private $templating;
    private $appTheme;

    public function __construct(
        MailerInterface $mailer,
        string $mailSender,
        Environment $templating,
        string $appTheme
    ) {
        $this->mailer = $mailer;
        $this->mailSender = $mailSender;
        $this->templating = $templating;
        $this->appTheme = $appTheme ?: 'default';
    }

    /**
     * Envoie d'un mail.
     *
     * @throws TransportExceptionInterface
     * @throws RfcComplianceException
     */
    public function sendHtmlMail(array $recipients, string $subject, string $mailTemplate, array $mailParameter, array $pdfAttachments = [], array $subjectParameters = [])
    {
        try {
            if (!empty($subjectParameters)) {
                $customSubject = $this->templating->render($this->appTheme.'/emails/subject.html.twig', [
                    'appName' => $subjectParameters['appName'],
                    'userName' => $subjectParameters['userName'],
                    'firstName' => $subjectParameters['firstName'],
                    'useName' => $subjectParameters['useName'],
                    'status' => $subjectParameters['status'],
                ]);

                if (!empty($customSubject)) {
                    $subject = $customSubject;
                }
            }

            $email = (new TemplatedEmail())
                ->from($this->mailSender)
                ->subject($subject)
                ->htmlTemplate($mailTemplate)
                ->context($mailParameter);
        } catch (RfcComplianceException $exception) {
            throw $exception;
        }

        foreach ($pdfAttachments as $name => $attachment) {
            $email->attach($attachment, $name, 'application/pdf');
        }

        foreach ($recipients as $recipient) {
            $email->addTo($recipient);
        }

        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $exception) {
            throw $exception;
        }
    }
}
