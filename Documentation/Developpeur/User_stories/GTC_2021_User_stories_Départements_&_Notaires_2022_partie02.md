# User stories Départements & Notaires 2022 - partie 2

Ce document présente les *user stories* et tests d'acceptation pour les développements à réaliser sur
Départements & Notaires (travaux postérieurs à la version 2.1.0).

Le numéro de chaque *user stories* correspond à son numéro de paragraphe dans le compte-rendu du GTC
(Groupe de Travail Collaboratif).

## 6.1. Import : gérer les bénéficiaires de plusieurs aides

*User story* saisie dans <https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/147>

### User Story 6.1-1 Importer des individus bénéficiaires de plusieurs aides

* **ETQ** opérateur technique de Départements & Notaire
* **Je souhaite :** importer des individus bénéficiaires de plusieurs aides
* **Afin de :** répondre au besoin métier

Remarques :

* [D&N v1](https://gitlab.adullact.net/departements-notaires/departements-notaires) gérait ce cas de figure.
  C'est certainement un oubli fonctionnel lors de la réécriture de la v2, mais les départements le voient comme une
  régression, voire un élément bloquant pour passer de la v1 à la v2
* un bénéficiaire peut cumuler plus de 100 aides (!). Nous posons une valeur maximale à 999
* Le comportement du moteur de recherche en présence de plusieurs aides doit reprendre celui de la v1.

### Tests d'acceptation --> pour tous les tests d'acceptation 6.1-xx

1. Utiliser le [jeu de données de test, de la branche *
   US61_aides_multiples*](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/US61_aides_multiples/appli_sf/imports/donnees_test.sql)
2. Se connecter en tant qu'Administrateur.
3. Dans les Paramètres de l'application, rubrique *Paramètres de recherche* :
    * La recherche Soundex est **décochée**
    * Le paramètre *Précision de la date de naissance* doit être réglé à :
      *Jour et mois différents donnent ambigu au lieu d'inconnu*.
    * Le paramètre *Prénom différent donne ambigu au lieu de connu* est **décoché**
    * Le paramètre *Recherche par nom* a pour valeur *Nom d'usage*
4. Il est à noter que les modèles par défaut de *courriel* et de *PDF*, pour une personne connue, indiquent désormais
   le nombre d'aide(s) récupérable(s), non récupérable(s) ou en cours d'instruction.

### Tests d'acceptation - 6.1-1-A Documentation Import

La documentation est :

* à jour,
* vérifiées fonctionnellement,
* gérant les imports avec ou sans bénéficiaires d'aides multiples chacune des pages
    * [Import individus](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Operateur/Import-individus.md)
    * [Import individus - test et vérifications](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Operateur/Import-individus-test-verifications.md)
    * [Mise à jour](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Operateur/mise-a-jour.md)

### Tests d'acceptation - 6.1-1-B Import avec bénéficiaires d'aides multiples

* Je transforme au format CSV le fichier
  [jeu de données de test, de la branche *
  US61_aides_multiples*](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/US61_aides_multiples/appli_sf/imports/donnees_test.sql)
* Je dérouler
  la [documentation d'import](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Operateur/Import-individus.md)
* je m'assurer que tous les individus sont importés sans erreurs ni avertissements.
* je vérifie que les individus suivants sont présents chacun plusieurs fois dans la table `individu`:
    * Francine de la Rochebonnefoy
    * Pierre Mayou
    * Lucette Perron
    * Antoine Perron
    * Sophie Perron
    * Berthe Goitreux
    * Émile Carabignac
    * Janine Peyroul
    * Patricia Patate
    * Dalal Syrie
    * Vasco Boisséson
    * Joseph Couvreur
    * Armand Garan-Servier
    * Pierre-Henri Garan-Servier
    * Enguerrand Brousse
    * François Civrac
    * Bastien Legoumier
    * Arno Nimousse
    * Jean-Chi Moutardier
    * Etienne Buset
    * Blandine Apatou
    * Clémence Kousou
    * Bernard Perron
    * Anita Bonny

### Tests d'acceptation - 6.1-1-C Tests automatisés des tests d'acceptations

* Les tests d'acceptation sont automatisés et exécutés dans la CI

### User Story - 6.1-2 Documentation Moteur de recherche

* **ETQ** agent du Conseil Départemental
* **Je souhaite** trouver, dans la documentation du moteur de recherche, la description de toute
  la combinatoire des aides pour un individu (cf tableau ci-dessous)
* **Afin** d'avoir une vision synthétique et complète des différents cas de figure

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|   A   |      0      |        0        |           0            |
|   B   |      n      |        n        |           0            |
|   C   |      0      |        n        |           0            |
|   D   |      n      |        0        |           0            |
| ----- | ----------  |   ----------    |       ----------       |
|   E   |      0      |        0        |           n            |
|   F   |      n      |        n        |           n            |
|   G   |      0      |        n        |           n            |
|   H   |      n      |        0        |           n            |

n est supérieur ou égal à 1

### Tests d'acceptation - 6.1-2 Cas "A" : Recherche de bénéficiaire sans aucune aide

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|   A   |      0      |        0        |           0            |

Le cas A n'a pas d'existence technique dans Départements & Notaires (du moins en version 2.0, 2.1 ou 2.2).
Le champ `code_aide` accepte trois valeurs :

* `1SEXTREC` (aide récupérable)
* `1SEXTNONR` (aide non récupérable)
* champs vide pour une aide en cours d'instruction

Ceci étant posé, le cas A ne se présente pas.

Le jour où les valeurs du champ `code_aide` évolueront, nous pourrons revisiter ce cas. Ce test d'acceptation est
laissé ici par souci de complétude, et pour faciliter la compréhension de la combinatoire des valeurs.

### Tests d'acceptation - 6.1-2 Cas "B1"

Cas B1 : Recherche de bénéficiaire avec 2 aides (une non récupérable et une récupérable)

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|  B1   |      1      |        1        |           0            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Lucette
    * Nom : Perron
    * Date de naissance : 01/01/1905
3. La page de résultat de recherche affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient : `1 aide récupérable` et `1 aide non récupérable`
6. Le courriel reçu par le Notaire ne contient pas "0 aide en cours d'instruction"
7. Le PDF (en pièce jointe du courriel) contient : `1 aide récupérable` et `1 aide non récupérable`
8. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "B2"

Cas B2 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables et 1 récupérable)

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|  B2   |      2      |        1        |           0            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Janine
    * Nom : Peyroul
    * Date de naissance : 01/01/1910
3. La page de résultat de recherche affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient : `2 aides récupérables` et `1 aide non récupérable`
6. Le courriel reçu par le Notaire ne contient pas "0 aide en cours d'instruction"
7. Le PDF (en pièce jointe du courriel) contient : `2 aides récupérables` et `1 aide non récupérable`
8. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "B3"

Cas B3 : Recherche de bénéficiaire avec plusieurs aides (1 non récupérable et 2 récupérables)

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|  B3   |      1      |        2        |           0            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Patricia
    * Nom : Patate
    * Date de naissance : 01/01/1911
3. La page de résultat de recherche affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient : `1 aide récupérable` et `2 aides non récupérables`
6. Le courriel reçu par le Notaire ne contient pas "0 aide en cours d'instruction"
7. Le PDF (en pièce jointe du courriel) contient : `1 aide récupérable` et `2 aides non récupérables`
8. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "B4"

Cas B4 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables et 2 récupérables)

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|  B4   |      2      |        2        |           0            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Dalal
    * Nom : Syrie
    * Date de naissance : 01/01/1912
3. La page de résultat de recherche affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient : `2 aides récupérables` et `2 aides non récupérables`
6. Le courriel reçu par le Notaire ne contient pas "0 aide en cours d'instruction"
7. Le PDF (en pièce jointe du courriel) contient : `2 aides récupérables` et `2 aides non récupérables`
8. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "C1"

Cas C1 : Recherche de bénéficiaire avec 1 aide non récupérables

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|   C1  |      0      |        1        |           0            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Maesson
    * Nom : ALOUANE
    * Date de naissance : 03/12/1954
3. La page de résultat de recherche affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient : `1 aide non récupérable`
6. Le courriel reçu par le Notaire ne contient pas "0 aide récupérable", ni "0 aide en cours d'instruction"
7. Le PDF (en pièce jointe du courriel) contient : `1 aide non récupérable`
8. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide récupérable", ni "0 aide en cours d'instruction"

**Notes :**

* cette *user story* est partiellement implémentée dans le test
  fonctionnel [`4-search.spec.js` ligne 36](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/US61_aides_multiples/appli_sf/tests/E2E/cypress/integration/FullDB/4-search.spec.js#L36)
* Ce test fonctionnel doit être mis à jour au regard cette *user story*

### Tests d'acceptation - 6.1-2 Cas "C2"

Cas C2 : Recherche de bénéficiaire avec 2 aides non récupérables

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|   C2  |      0      |        2        |           0            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : Bordeau
    * Date du certificat de décès : 12/02/2020
    * Prénom : Francine
    * Nom : de la Rochebonnefoy
    * Date de naissance : 01/01/1903
3. La page de résultat de recherche affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient : `2 aides non récupérables`
6. Le courriel reçu par le Notaire ne contient pas "0 aide récupérable", ni "0 aide en cours d'instruction"
7. Le PDF (en pièce jointe du courriel) contient : `2 aides non récupérables`
8. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide récupérable", ni "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "D1"

Cas D1 : Recherche de bénéficiaire avec 1 aide récupérable

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|   D1  |      1      |        0        |           0            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Aimé
    * Nom : OLMO
    * Date de naissance : 06/12/1977
3. La page de résultat de recherche affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient : `1 aide récupérable`
6. Le courriel reçu par le Notaire ne contient pas "0 aide non récupérable", ni "0 aide en cours d'instruction"
7. Le PDF (en pièce jointe du courriel) contient : `1 aide récupérable`
8. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide non récupérable", ni "0 aide en cours d'instruction"

**Notes :**

* cette *user story* est partiellement implémentée dans le test
  fonctionnel [`4-search.spec.js` ligne 21](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/US61_aides_multiples/appli_sf/tests/E2E/cypress/integration/FullDB/4-search.spec.js#L21)
* Ce test fonctionnel doit être mis à jour au regard cette *user story*

### Tests d'acceptation - 6.1-2 Cas "D2"

Cas D2 : Recherche de bénéficiaire avec 2 aides récupérables

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|  D2   |      2      |        0        |           0            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Pierre
    * Nom : Mayou
    * Date de naissance : 01/01/1904
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient : `2 aides récupérables`
6. Le courriel reçu par le Notaire ne contient pas "0 aide non récupérable", ni "0 aide en cours d'instruction"
7. Le PDF (en pièce jointe du courriel) contient : `2 aides récupérables`
8. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide non récupérable", ni "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "E1"

Cas E1 : Recherche de bénéficiaire avec 1 aide en cours d'instruction

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|  E1   |      0      |        0        |           1            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Berthe
    * Nom : Goitreux
    * Date de naissance : 01/01/1908
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient : `1 aide en cours d'instruction`
6. Le courriel reçu par le Notaire ne contient pas "0 aide récupérable", ni "0 aide non récupérable"
7. Le PDF (en pièce jointe du courriel) contient : `1 aide en cours d'instruction`
8. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide récupérable", ni "0 aide non récupérable"

### Tests d'acceptation - 6.1-2 Cas "E2"

Cas E2 : Recherche de bénéficiaire avec 2 aides en cours d'instruction

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|  E2   |      0      |        0        |           2            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Émile
    * Nom : Carabignac
    * Date de naissance : 01/01/1909
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient : `2 aides en cours d'instruction`
6. Le courriel reçu par le Notaire ne contient pas "0 aide récupérable", ni "0 aide non récupérable"
7. Le PDF (en pièce jointe du courriel) contient : `2 aides en cours d'instruction`
8. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide récupérable", ni "0 aide non récupérable"

### Tests d'acceptation - 6.1-2 Cas "F1"

Cas F1 : Recherche de bénéficiaire avec 3 aides (1 non récupérable, 1 récupérable et 1 en cours d'instruction)

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|  F1   |      1      |        1        |           1            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Bastien
    * Nom : Legoumier
    * Date de naissance : 01/01/1919
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient :
    - `1 aide récupérable`
    - `1 aide non récupérable`
    - `1 aide en cours d'instruction`
6. Le PDF (en pièce jointe du courriel) contient :
    - `1 aide récupérable`
    - `1 aide non récupérable`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "F2"

Cas F2 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables, 2 récupérables et 1 en cours d'instruction)

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|  F2   |      2      |        2        |           1            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Jean-Chi
    * Nom : Moutardier
    * Date de naissance : 01/01/1921
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient :
    - `2 aides récupérables`
    - `2 aides non récupérables`
    - `1 aide en cours d'instruction`
6. Le PDF (en pièce jointe du courriel) contient :
    - `2 aides récupérables`
    - `2 aides non récupérables`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas F3

Cas F3 : Recherche de bénéficiaire avec plusieurs aides (1 non récupérable, 2 récupérables et 1 en cours d'instruction)

| Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:----:|:-----------:|:---------------:|:----------------------:|
|  F3  |      1      |        2        |           1            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Etienne
    * Nom : Buset
    * Date de naissance : 01/01/1922
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient :
    - `1 aide récupérable`
    - `2 aides non récupérables`
    - `1 aide en cours d'instruction`
6. Le PDF (en pièce jointe du courriel) contient :
    - `1 aide récupérable`
    - `2 aides non récupérables`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "F4"

Cas F4 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables, 1 récupérable et 1 en cours d'instruction)

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| F4  |      2      |        1        |           1            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Blandine
    * Nom : Apatou
    * Date de naissance : 01/01/1923
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient :
    - `2 aides récupérables`
    - `1 aide non récupérable`
    - `1 aide en cours d'instruction`
6. Le PDF (en pièce jointe du courriel) contient :
    - `2 aides récupérables`
    - `1 aide non récupérable`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas F5

Cas F5 : Recherche de bénéficiaire avec plusieurs aides (1 non récupérable, 1 récupérable et 2 en cours d'instruction)

|  Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:-----:|:-----------:|:---------------:|:----------------------:|
|  F5   |      1      |        1        |           2            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Clémence
    * Nom : Kousou
    * Date de naissance : 01/01/1924
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient :
    - `1 aide récupérable`
    - `1 aide non récupérable`
    - `2 aides en cours d'instruction`
6. Le PDF (en pièce jointe du courriel) contient :
    - `1 aide récupérable`
    - `1 aide non récupérable`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas F6

Cas F6 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables, 2 récupérables et 2 en cours d'instruction)

| Cas | Récupérable | Non-récupérable | En cours d'instruction |
|:---:|:-----------:|:---------------:|:----------------------:|
| F6  |      2      |        2        |           2            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Arno
    * Nom : Nimousse
    * Date de naissance : 01/01/1920
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient :
    - `2 aides récupérables`
    - `2 aides non récupérables`
    - `2 aides en cours d'instruction`
6. Le PDF (en pièce jointe du courriel) contient :
    - `2 aides récupérables`
    - `2 aides non récupérables`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas F7

Cas F7 : Recherche de bénéficiaire avec plusieurs aides (1 non récupérable, 2 récupérables et 2 en cours d'instruction)

| Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:----:|:-----------:|:---------------:|:----------------------:|
|  F7  |      1      |        2        |           2            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Bernard
    * Nom : Perron
    * Date de naissance : 01/01/1925
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient :
    - `1 aide récupérable`
    - `2 aides non récupérable`
    - `2 aides en cours d'instruction`
6. Le PDF (en pièce jointe du courriel) contient :
    - `1 aide récupérable`
    - `2 aides non récupérable`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas F8

Cas F8 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables, 1 récupérable et 2 en cours d'instruction)

| Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:----:|:-----------:|:---------------:|:----------------------:|
|  F8  |      2      |        1        |           2            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Anita
    * Nom : Bonny
    * Date de naissance : 01/01/1926
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire contient :
    - `2 aides récupérables`
    - `1 aide non récupérable`
    - `2 aides en cours d'instruction`
6. Le PDF (en pièce jointe du courriel) contient :
    - `2 aides récupérables`
    - `1 aide non récupérable`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "G1"

Cas G1 : Recherche de bénéficiaire avec 2 aides (une non récupérable et une en cours d'instruction)

| Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:----:|:-----------:|:---------------:|:----------------------:|
|  G1  |      0      |        1        |           1            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Sophie
    * Nom : Perron
    * Date de naissance : 01/01/1907
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire ne contient pas "0 aide récupérable"
6. Le courriel reçu par le Notaire contient :
    - `1 aide non récupérable`
    - `1 aide en cours d'instruction`
7. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide récupérable"
8. Le PDF (en pièce jointe du courriel) contient :
    - `1 aide non récupérable`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "G2"

Cas G2 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables et 1 en cours d'instruction)

| Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:----:|:-----------:|:---------------:|:----------------------:|
|  G2  |      0      |        2        |           1            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Vasco
    * Nom : Boisséson
    * Date de naissance : 01/01/1913
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire ne contient pas "0 aide récupérable"
6. Le courriel reçu par le Notaire contient :
    - `2 aides non récupérables`
    - `1 aide en cours d'instruction`
7. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide récupérable"
8. Le PDF (en pièce jointe du courriel) contient :
    - `2 aides non récupérables`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "G3"

Cas G3 : Recherche de bénéficiaire avec plusieurs aides (1 non récupérable et 2 en cours d'instruction)

| Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:----:|:-----------:|:---------------:|:----------------------:|
|  G3  |      0      |        1        |           2            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Joseph
    * Nom : Couvreur
    * Date de naissance : 01/01/1914
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire ne contient pas "0 aide récupérable"
6. Le courriel reçu par le Notaire contient :
    - `1 aide non récupérable`
    - `2 aides en cours d'instruction`
7. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide récupérable"
8. Le PDF (en pièce jointe du courriel) contient :
    - `1 aide non récupérable`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "G4"

Cas G4 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables et 2 en cours d'instruction)

| Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:----:|:-----------:|:---------------:|:----------------------:|
|  G4  |      0      |        2        |           2            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Armand
    * Nom : Garan-Servier
    * Date de naissance : 01/01/1915
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire ne contient pas "0 aide récupérable"
6. Le courriel reçu par le Notaire contient :
    - `2 aides non récupérables`
    - `2 aides en cours d'instruction`
7. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide récupérable"
8. Le PDF (en pièce jointe du courriel) contient :
    - `2 aides non récupérables`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "H1"

Cas H1 : Recherche de bénéficiaire avec 2 aides (une récupérable et une en cours d'instruction)

| Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:----:|:-----------:|:---------------:|:----------------------:|
|  H1  |      1      |        0        |           1            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Antoine
    * Nom : Perron
    * Date de naissance : 01/01/1906
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire ne contient pas "0 aide non récupérable"
6. Le courriel reçu par le Notaire contient :
    - `1 aide récupérable`
    - `1 aide en cours d'instruction`
7. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide non récupérable"
8. Le PDF (en pièce jointe du courriel) contient :
    - `1 aide récupérable`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "H2"

Cas H2 : Recherche de bénéficiaire avec plusieurs aides (2 récupérables et 1 en cours d'instruction)

| Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:----:|:-----------:|:---------------:|:----------------------:|
|  H2  |      2      |        0        |           1            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Pierre-Henri
    * Nom : Garan-Servier
    * Date de naissance : 01/01/1916
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire ne contient pas "0 aide non récupérable"
6. Le courriel reçu par le Notaire contient :
    - `2 aides récupérables`
    - `1 aide en cours d'instruction`
7. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide non récupérable"
8. Le PDF (en pièce jointe du courriel) contient :
    - `2 aides récupérables`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "H3"

Cas H3 : Recherche de bénéficiaire avec plusieurs aides (1 récupérable et 2 en cours d'instruction)

| Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:----:|:-----------:|:---------------:|:----------------------:|
|  H3  |      1      |        0        |           2            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Enguerrand
    * Nom : Brousse
    * Date de naissance : 01/01/1917
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire ne contient pas "0 aide non récupérable"
6. Le courriel reçu par le Notaire contient :
    - `1 aide récupérable`
    - `2 aides en cours d'instruction`
7. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide non récupérable"
8. Le PDF (en pièce jointe du courriel) contient :
    - `1 aide récupérable`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "H4"

Cas H4 : Recherche de bénéficiaire avec plusieurs aides (2 récupérables et 2 en cours d'instruction)

| Cas  | Récupérable | Non-récupérable | En cours d'instruction |
|:----:|:-----------:|:---------------:|:----------------------:|
|  H4  |      2      |        0        |           2            |

1. Se connecter en tant que Notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : François
    * Nom : Civrac
    * Date de naissance : 01/01/1918
3. La page de résultat de recherche
   affiche :

   ```text
   Cette personne est connue de nos services, un message électronique vous est envoyé avec les informations relatives à votre demande.
   ```

4. Le courriel reçu par le Notaire contient : `est connue du département`
5. Le courriel reçu par le Notaire ne contient pas "0 aide non récupérable"
6. Le courriel reçu par le Notaire contient :
    - `2 aides récupérables`
    - `2 aides en cours d'instruction`
7. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide non récupérable"
8. Le PDF (en pièce jointe du courriel) contient :
    - `2 aides récupérables`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Automatisation des tests d'acceptations de tous les cas

* Les tests d'acceptation sont automatisés et exécutés dans la CI, et ce, pour tous les 27 cas (de A à H4).

### User Story - 6.1-3 Instructeur et aide en cours d'instruction

* **ETQ** administrateur,
* **Je souhaite** affecter à un instructeur la gestion des réponses "aide en cours d'instruction", éventuellement
  combinée aux autres résultats (Récupération, Indus, Ambigu)
* **Afin** répartir la charge de travail entre instructeurs

### Tests d'acceptation - 6.1-3 Cas A Instructeur avec "aide en cours d'instruction" uniquement

* Se connecter en tant qu'admin
* Aller dans Administration > Gérer les Instructeurs
* cliquer sur Ajouter un instructeur
* Saisir les informations de l'instructeur, et en "Type de réponse", ne lui affecter que "aide en cours d'instruction"

### Tests d'acceptation - 6.1-3 Cas B Instructeur avec "aide en cours d'instruction" & Ambigu

* Se connecter en tant qu'admin
* Aller dans Administration > Gérer les Instructeurs
* cliquer sur Ajouter un instructeur
* Saisir les informations de l'instructeur, et en "Type de réponse", lui affecter :
    * "Aide en cours d'instruction"
    * "Ambigu"

### Tests d'acceptation - 6.1-3 Automatisation des tests d'acceptations

* Les tests d'acceptation sont automatisés et exécutés dans la CI

## 6.2. Différencier les courriers selon le type d’aide ================================================================

*User story* saisie dans <https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/148>

### User Story 6.2

La *User Story* 6.2 est doit être couverte par la *User Story* 7.3

## 6.3. Gestion des individus avec un code d’aide vide (en cours d’instruction) ========================================

*User story* saisie dans <https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/149>

### Note

Le point 6.3 est **en partie** couvert par les *user story* 6.1

### User Story 6.3-01 historique de recherche Notaire

* **ETQ** Notaire,
* **Je souhaite** voir dans mon historique de recherche :
    * les recherches ayant pour résultat *Aide en cours d'instruction*
    * pour une recherche donnant plusieurs résultats, tous les résultats de ladite recherche
* **Afin** de retrouver une recherche par son type de réponse.

### Test d'acceptation 6.3-01 historique de recherche Notaire

1. Se connecter en tant que notaire
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Arno
    * Nom : Nimousse
    * Date de naissance : 01/01/1920
3. Se rendre dans *Mes précédentes recherches*
4. Le champ *Type de réponse* doit proposer (en plus des valeurs actuelles), la valeur *Aide en cours d'instruction*
5. Dans le tableau, à la ligne correspondant à la recherche, la colonne *Réponse* doit afficher les 3 résultats i.e. :
    * Récupération
    * Indus probable
    * Aide en cours d'instruction

### User Story 6.3-02 historique de recherche Agent ou Administrateur

* **ETQ** Agent ou Administrateur,
* **Je souhaite** voir dans *Liste des recherches* :
    * les recherches ayant pour résultat *Aide en cours d'instruction*
    * pour une recherche donnant plusieurs résultats, tous les résultats de ladite recherche
* **Afin** de retrouver une recherche par son type de réponse.

### Test d'acceptation 6.3-02 historique de recherche Agent ou Administrateur

1. Se connecter en tant que Agent ou Administrateur
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Arno
    * Nom : Nimousse
    * Date de naissance : 01/01/1920
3. Se rendre dans *Liste des recherches*
4. Le champ *Type de réponse* doit proposer (en plus des valeurs actuelles), la valeur *Aide en cours d'instruction*
5. Dans le tableau, à la ligne correspondant à la recherche, la colonne *Réponse* doit afficher les 3 résultats i.e. :
    * Récupération
    * Indus probable
    * Aide en cours d'instruction

### User Story 6.3-03 statistiques

* **ETQ** Agent ou Administrateur,
* **Je souhaite** voir dans les *statistiques* :
    * dans le tableau, le nombre de recherches ayant eu pour résultat *Aide en cours d'instruction*
    * dans le graphique, le nombre de recherches ayant eu pour résultat *Aide en cours d'instruction*
* **Afin** d'avoir une répartition des résultats

### Test d'acceptation 6.3-03 statistiques

1. Se connecter en tant que Agent ou Administrateur
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Arno
    * Nom : Nimousse
    * Date de naissance : 01/01/1920
3. Se rendre dans *statistiques*
4. Le tableau doit afficher une ligne *Aides en cours d'instruction* sous la ligne *Indus probables*
5. Le graphique doit présenter aussi les *Aides en cours d'instruction*

### Tests d'acceptation - 6.3-04 Automatisation des tests d'acceptations

* Les tests d'acceptation sont automatisés et exécutés dans la CI
