#!/usr/bin/env groovy

library 'atolcd-jenkins'

pipeline {
  options {
    disableConcurrentBuilds()
    buildDiscarder(logRotator(numToKeepStr: '10'))
  }
  agent any
  environment {
    PROJECT = bashEval('./jenkins-env.sh PROJECT')
    ZULIP_STREAM = bashEval('./jenkins-env.sh ZULIP_STREAM')
    NEXUS_REPO = bashEval('./jenkins-env.sh NEXUS_REPO')
    TYPE = bashEval('./jenkins-env.sh TYPE')
    VERSION = bashEval('./jenkins-env.sh VERSION')
    VERSION_IS_FOR_TESTING = bashEval('./jenkins-env.sh VERSION_IS_FOR_TESTING')
    BASE_URL_UPLOAD = bashEval('./jenkins-env.sh BASE_URL_UPLOAD')
    APP_FILENAME_TAR = bashEval('./jenkins-env.sh APP_FILENAME_TAR')
    LATEST_URL = bashEval('./jenkins-env.sh LATEST_URL')
    LATEST_APP_FILENAME = bashEval('./jenkins-env.sh LATEST_APP_FILENAME')
    PHP_VERSION = bashEval('./jenkins-env.sh PHP_VERSION')
    COMPOSER_VERSION = bashEval('./jenkins-env.sh COMPOSER_VERSION')
    NODE_VERSION = bashEval('./jenkins-env.sh NODE_VERSION')
    ENVIRONMENT = 'jenkins-release'
  }
  stages {
    stage('Notify') {
      steps {
        zulipSend stream: "$ZULIP_STREAM", topic: "jenkins-$TYPE", message: ":gear: Start build $PROJECT : $VERSION"
      }
    }
    stage('Build PHP') {
      steps {
        phpBuildAndInsideDocker(buildDir: 'docker/php', buildArgs: [PHP_VERSION: env.PHP_VERSION, COMPOSER_VERSION: env.COMPOSER_VERSION, ENVIRONMENT: env.ENVIRONMENT]) {
          dir('appli_sf') {
            sh 'composer install --no-progress --no-dev --no-suggest --no-interaction --no-scripts --optimize-autoloader'
          }
        }
      }
    }
    stage('Build Node') {
      steps {
        nodejsInsideDocker(imageVersion: NODE_VERSION) {
          dir('appli_sf') {
            sh 'sed -i "s@https://registry.yarnpkg.com/@$(npm config get registry)@" yarn.lock'
            sh 'yarn && yarn run build'
          }
        }
      }
    }
    stage('Prepare files') {
      steps {
        sh 'ls final > /dev/null 2>&1 && rm -fr final || true'
        sh 'mkdir final && cp -RLp appli_sf final/app || true'
        sh 'echo "$VERSION" > final/build-version'
        sh 'rm -rf final/app/node_modules || true'
        sh 'chmod u+x final/app/bin/*'
      }
    }
    stage('Package') {
      steps {
        sh 'cd final && tar -chpzf ../$APP_FILENAME_TAR *'
      }
    }
    stage('Upload') {
      steps {
        // TODO: utiliser publishRawClientNexus ou publishRawNexus
        withCredentials([usernameColonPassword(credentialsId: 'nexus3-jenkins', variable: 'NEXUS3_AUTH')]) {
          sh 'curl -v --user "$NEXUS3_AUTH" --upload-file ./$APP_FILENAME_TAR $BASE_URL_UPLOAD/tar/$APP_FILENAME_TAR'
        }
      }
    }
    stage('Push version in latest file') {
      when {
        environment name: 'VERSION_IS_FOR_TESTING', value: '0';
      }
      steps {
        // TODO: utiliser publishRawClientNexus ou publishRawNexus
        withCredentials([usernameColonPassword(credentialsId: 'nexus3-jenkins', variable: 'NEXUS3_AUTH')]) {
          sh 'echo "${VERSION}" > version-latest.txt'
          sh 'curl -v --user "$NEXUS3_AUTH" --upload-file ./version-latest.txt $LATEST_URL/$LATEST_APP_FILENAME'
        }
      }
    }
  }
  post {
    always {
      deleteDir()
      zulipNotification smartNotification: "${TYPE=='release'?'disabled':'enabled'}", stream: "${ZULIP_STREAM}", topic: "jenkins-${TYPE}"
    }
  }
}
