<?php

namespace App\Service;

/**
 * Class VersionService.
 */
class VersionService
{
    private $file;

    public function __construct(string $projectDir)
    {
        $this->file = $projectDir.'/composer.json';
    }

    /**
     * Retourne la version courante de l'application.
     */
    public function getVersion(): string
    {
        $json = file_get_contents($this->file);
        $vars = json_decode($json, true);

        return $vars['version'];
    }
}
