<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Platforms\MariaDBPlatform;
use Doctrine\DBAL\Platforms\MySQLPlatform;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20220818124918 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Change type_response in log_recherche table to allow an array, and transform old int values in array';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
         $this->abortIf(
             !($this->connection->getDatabasePlatform() instanceof MySQLPlatform) &&
             !($this->connection->getDatabasePlatform() instanceof MariaDbPlatform),
             'Migration can only be executed safely on \'mysql\'.'
         );

        $this->addSql('ALTER TABLE log_recherche CHANGE type_reponse type_reponse LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\'');
        $this->addSql("UPDATE log_recherche SET log_recherche.type_reponse = CONCAT('\[', log_recherche.type_reponse, '\]')");
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
         $this->abortIf(
             !($this->connection->getDatabasePlatform() instanceof MySQLPlatform) &&
             !($this->connection->getDatabasePlatform() instanceof MariaDbPlatform),
             'Migration can only be executed safely on \'mysql\'.'
         );

        $this->addSql("UPDATE log_recherche SET log_recherche.type_reponse = REPLACE(log_recherche.type_reponse, '\[', '')");
        $this->addSql("UPDATE log_recherche SET log_recherche.type_reponse = REPLACE(log_recherche.type_reponse, '\]', '')");
        $this->addSql('ALTER TABLE log_recherche CHANGE type_reponse type_reponse INT');
    }
}
