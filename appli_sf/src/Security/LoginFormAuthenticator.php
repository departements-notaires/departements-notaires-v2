<?php

namespace App\Security;

use App\Data\SettingsData;
use App\Entity\User;
use App\Entity\UserLog;
use App\Service\SettingsDataService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractLoginFormAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Http\SecurityRequestAttributes;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Contracts\Translation\TranslatorInterface;

class LoginFormAuthenticator extends AbstractLoginFormAuthenticator
{
    use TargetPathTrait;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UrlGeneratorInterface
     */
    private $urlGenerator;

    /**
     * @var CsrfTokenManagerInterface
     */
    private $csrfTokenManager;

    /**
     * @var SettingsData
     */
    private $settingsData;

    /**
     * @var TranslatorInterface
     */
    private $translator;

    /**
     * @var TokenGeneratorInterface
     */
    private $tokenGenerator;

    public function __construct(
        EntityManagerInterface $entityManager,
        UrlGeneratorInterface $urlGenerator,
        CsrfTokenManagerInterface $csrfTokenManager,
        SettingsDataService $settingsDataService,
        TranslatorInterface $translator,
        TokenGeneratorInterface $tokenGenerator,
    ) {
        $this->entityManager = $entityManager;
        $this->urlGenerator = $urlGenerator;
        $this->csrfTokenManager = $csrfTokenManager;
        $this->settingsData = $settingsDataService->getData();
        $this->translator = $translator;
        $this->tokenGenerator = $tokenGenerator;
    }

    public function supports(Request $request): bool
    {
        return 'app_login' === $request->attributes->get('_route')
            && $request->isMethod('POST');
    }

    public function getCredentials(Request $request): array
    {
        $credentials = [
            'username' => $request->request->get('username'),
            'password' => $request->request->get('password'),
            'csrf_token' => $request->request->get('_csrf_token'),
        ];
        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['username']
        );

        return $credentials;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        $this->addUserLog(false, $token->getUser());

        if ($targetPath = $this->getTargetPath($request->getSession(), $firewallName)) {
            return new RedirectResponse($targetPath);
        }

        return new RedirectResponse($this->urlGenerator->generate('index'));
    }

    /**
     * Override to change what happens after a bad username/password is submitted.
     *
     * @return RedirectResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
        if ($request->hasSession()) {
            $request->getSession()->set(Security::AUTHENTICATION_ERROR, $exception);
        }
        $credentials = $this->getCredentials($request);

        $url = $this->getLoginUrl($request);
        $token = new CsrfToken('authenticate', $credentials['csrf_token']);
        if (!$this->csrfTokenManager->isTokenValid($token)) {
            throw new InvalidCsrfTokenException();
        }

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $credentials['username']]);

        if ($user) {
            if ($user->isDisabled() || 0 !== $this->settingsData->getConnectionAttempts()
                && $user->getNbLoginAttempts() >= $this->settingsData->getConnectionAttempts()) {
                $this->addUserLog(true, $user);
            }
        }

        return new RedirectResponse($url);
    }

    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate('app_login');
    }

    /**
     * Ajoute la connexion dans les logs de connexion.
     */
    private function addUserLog(bool $failure, User|UserInterface|null $user = null): void
    {
        if ($user) {
            $userLog = new UserLog();

            $userLog->setDate(new \DateTime());
            $userLog->setUser($user);
            $userLog->setFailure($failure);

            $this->entityManager->persist($userLog);
            $this->entityManager->flush();
        }
    }

    public function authenticate(Request $request): Passport
    {
        $credentials = $this->getCredentials($request);

        $request->getSession()->set(SecurityRequestAttributes::LAST_USERNAME, $credentials['username']);

        /** @var User $user */
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['username' => $credentials['username']]);

        if ($user) {
            if ($user->isDisabled()
                || 0 !== $this->settingsData->getConnectionAttempts()
                && $user->getNbLoginAttempts() >= $this->settingsData->getConnectionAttempts()) {
                $token = $this->tokenGenerator->generateToken();

                $user->setDisabled(true);
                $user->setTokenReset($token);
                $this->entityManager->flush();

                $resetUrl = $this->urlGenerator->generate(
                    'send_unlock_key',
                    ['token' => $token],
                    UrlGeneratorInterface::ABSOLUTE_URL
                );

                throw new CustomUserMessageAuthenticationException($this->translator->trans('login.accountBlocked', ['%link%' => $resetUrl]));
            }
        }

        return new Passport(
            new UserBadge($credentials['username']),
            new PasswordCredentials($credentials['password']),
            [
                new CsrfTokenBadge('authenticate', $credentials['csrf_token']),
            ]
        );
    }
}
