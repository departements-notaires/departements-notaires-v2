<?php

namespace App\Command;

use App\Entity\SearchLog;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class MigrationLogRecherche extends Command
{
    /**
     * @var SymfonyStyle
     */
    private $io;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var UserRepository
     */
    private $userRepository;

    public function __construct(UserRepository $userRepository, EntityManagerInterface $entityManager)
    {
        parent::__construct();

        $this->userRepository = $userRepository;
        $this->entityManager = $entityManager;
    }

    protected function configure()
    {
        $this
            ->setName('import:logs_recherche_v1')
            ->setDescription('Migrer les logs de recherche de la v1 vers la v2')
            ->setHelp(
                'Permet d\'importer un export CSV des logs de recherche de la v1 avec le chemin du fichier en paramètre.'
            )
            ->addArgument('file', InputArgument::REQUIRED, 'Export CSV des logs de recherche de la v1');
    }

    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $noInteraction = $input->getOption('no-interaction');
        $path = realpath(__DIR__.'/../../').'/'.$input->getArgument('file');
        $hasHeaders = false;

        if (!is_file($path)) {
            $this->io->newLine();
            $this->io->error('Le fichier '.$path.' n\'a pas été trouvé.');

            return Command::FAILURE;
        }

        $this->io->title('Migration des logs de recherche en version 2');
        $this->io->caution(
            'Il est conseillé de faire une sauvegarde de la base de données avant de lancer ce script '.
            'si elle contient des données à conserver. '
        );
        $continue = $this->io->confirm(
            'Il est obligatoire d\'avoir importé les utilisateurs et les individus avant d\'importer les logs de recherche. '.
            'Voulez-vous continuez ?',
            false
        );

        if ($continue || $noInteraction) {
            $this->io->section("Préparation de l'import des logs de recherche");
            $this->io->writeln('Fichier à importer : '.realpath($path));

            $contentFile = file($path, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
            $nbLogsSearchToImport = count($contentFile);

            if (false !== strpos($contentFile[0], 'login')) {
                --$nbLogsSearchToImport;
                $hasHeaders = true;
            }

            $this->io->writeln($nbLogsSearchToImport.' logs de recherche à importer.');

            $this->io->progressStart($nbLogsSearchToImport);

            foreach ($contentFile as $lineNb => $line) {
                if (0 === $lineNb && $hasHeaders) {
                    continue;
                }

                $this->addLogSearch($line);
                $this->io->progressAdvance();
            }

            $this->io->progressFinish();
            $this->io->success('Import des logs de recherche');
        }

        return Command::SUCCESS;
    }

    /**
     * Ajout d'un log de recherche à partir d'une ligne CSV (format v1).
     *
     * @throws \Exception
     */
    protected function addLogSearch(string $line): bool
    {
        $log = new SearchLog();
        $lineValues = str_getcsv($line, ';');

        if ('id' != $lineValues[0]) {
            $log
                ->setUser($this->userRepository->findOneBy(['username' => $lineValues[2]]))
                ->setPerson((int) trim($lineValues[14]) ?: null)
                ->setUseName(trim($lineValues[4]))
                ->setFirstNames([trim($lineValues[6])])
                ->setBirthDate(new \DateTime(trim($lineValues[7])))
                ->setDeathDate(new \DateTime(trim($lineValues[11])))
                ->setDeathLocation(trim($lineValues[12]))
                ->setDeathCertificateDate(new \DateTime(trim($lineValues[13])))
                ->setResponseType([trim($lineValues[10])])
                ->setSearchDate(new \DateTime(trim($lineValues[9])))
                ->setCivilName(trim($lineValues[5]));

            $this->entityManager->persist($log);
            $this->entityManager->flush();

            return true;
        }

        return false;
    }
}
