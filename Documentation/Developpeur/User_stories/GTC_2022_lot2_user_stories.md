# User stories Départements & Notaires - 7. Emails

## 7.1 - Pouvoir personnaliser le sujet d'un mail ===================================================

Ticket [Pouvoir personnaliser le sujet du mail](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/127)
.

La fonctionnalité demandée consiste à pouvoir personnaliser le sujet d'un email. Une documentation plus approfondie
quant aux tenants et aboutissants de celle-ci se situe dans le
dossier [Opérateur](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation/Operateur)
.

### User story - 7.1. Pouvoir personnaliser le sujet d’un mail

* **ETQ :** opérateur technique de Départements & Notaire,
* **Je souhaite :** personnaliser le sujet des courriels envoyés aux notaires (de la même manière que je peux
  personnaliser le contenu du courriel et des PDFs)
* **Afin de :** avoir une information immédiate sans devoir lire tout le message

### Pour tous les tests d'acceptation

1. Utiliser
   le [jeu de données de test](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/imports/donnees_test.sql)
2. Se connecter en tant que Administrateur
3. Dans les Paramètres de l'application, rubrique *Paramètres de recherche*, le paramètre
   *Précision de la date de naissance doit être réglé à : Jour et mois différents donnent ambigu au lieu d'inconnu*

### Test d'acceptation - 7.1-01 CONNU cas par défaut

1. Se connecter en tant que Notaire (dont le nom est "admin")
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : aimé
    * Nom obligatoire : Olmo
    * Date de naissance : 06/12/1977
3. Le mail reçu par le notaire doit avoir pour sujet :
   `[Départements et Notaires] admin - Succession Aimé Olmo - connu`

### Tests d'acceptation - 7.1-02 CONNU cas personnalisé

1. ETQ qu'opérateur technique, je modifie le modèle de sujet de mail pour une personne connue
2. J'y place le contenu: `[{{ appName }}] {{ status }} - {{ userName }} - Succession {{ firstName }} {{ useName }}`
3. Se connecter en tant que Notaire (dont le nom est "admin")
4. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : aimé
    * Nom obligatoire : Olmo
    * Date de naissance : 06/12/1977
5. Le mail reçu par le notaire doit avoir pour sujet :
   `[Départements et Notaires] connu - admin - Succession Aimé Olmo`

### Test d'acceptation - 7.1-03 INCONNU cas par défaut

1. Se connecter en tant que Notaire (dont le nom est "admin")
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Mickael
    * Nom obligatoire : Dupont
    * Date de naissance : 07/08/1986
3. Le mail reçu par le notaire doit avoir pour sujet :
   `[Départements et Notaires] admin - Succession Mickael Dupont - inconnu`

### Tests d'acceptation - 7.1-04 INCONNU cas personnalisé

1. ETQ qu'opérateur technique, je modifie le modèle de sujet de mail pour une personne inconnue
2. J'y place le contenu: `[{{ appName }}] {{ status }} - {{ userName }} - Succession {{ firstName }} {{ useName }}`
3. Se connecter en tant que Notaire (dont le nom est "admin")
4. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Mickael
    * Nom obligatoire : Dupont
    * Date de naissance : 07/08/1986
5. Le mail reçu par le notaire doit avoir pour sujet :
   `[Départements et Notaires] inconnu - admin - Succession Mickael Dupont`

### Test d'acceptation - 7.1-05 EN COURS D'INSTRUCTION cas par défaut

1. Se connecter en tant que Notaire (dont le nom est "admin")
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Yvette
    * Nom obligatoire : ROSA
    * Date de naissance : 08/12/1968
3. Le mail reçu par le notaire doit avoir pour sujet :
   `[Départements et Notaires] admin - Succession Yvette ROSA - en cours d'instruction`

### Tests d'acceptation - 7.1-06 EN COURS D'INSTRUCTION cas personnalisé

1. ETQ qu'opérateur technique, je modifie le modèle de sujet de mail pour une personne en cours d'instruction
2. J'y place le contenu: `[{{ appName }}] {{ status }} - {{ userName }} - Succession {{ firstName }} {{ useName }}`
3. Se connecter en tant que Notaire (dont le nom est "admin")
4. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Yvette
    * Nom obligatoire : ROSA
    * Date de naissance : 08/12/1968
5. Le mail reçu par le notaire doit avoir pour sujet :
   `[Départements et Notaires] en cours d'instruction - admin - Succession Yvette ROSA`

### Test d'acceptation - 7.1-07 AMBIGU cas par défaut

1. Se connecter en tant que Notaire (dont le nom est "admin")
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Clara
    * Nom obligatoire : BOURDIAU
    * Date de naissance : 01/09/1966
3. Le mail reçu par le notaire doit avoir pour sujet :
   `[Départements et Notaires] admin - Succession Clara BOURDIAU - ambigu`

### Tests d'acceptation - 7.1-08 AMBIGU cas personnalisé

1. ETQ qu'opérateur technique, je modifie le modèle de sujet de mail pour une personne ambiguë
2. J'y place le contenu: `[{{ appName }}] {{ status }} - {{ userName }} - Succession {{ firstName }} {{ useName }}`
3. Se connecter en tant que Notaire (dont le nom est "admin")
4. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Clara
    * Nom obligatoire : BOURDIAU
    * Date de naissance : 01/09/1966
5. Le mail reçu par le notaire doit avoir pour sujet :
   `[Départements et Notaires] ambigu - admin - Succession Clara BOURDIAU`

## 7.2 - Pouvoir personnaliser la signature de mail métier depuis le backoffice ===================================================

Ticket [Pouvoir personnaliser la signature de mail métier depuis le backoffice](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/128)
.

### User Story 7.2. Pouvoir personnaliser la signature mail métier depuis le backoffice

* **ETQ :** administrateur
* **Je souhaite :** pouvoir modifier la signature des mails métier directement depuis le backoffice
* **Afin de :** ne pas être obligé de modifier les modèles de mails (ce qui nécessite une intervention
  des équipes système du Département)

Le champs de personnalisation de la signature de mail métier se situera dans les paramètres de l'application, sous la
rubrique *Paramètres mail système*, dans *Paramètres mails métier*. Il aura pour nom *Signature métier*.

### Tests d'acceptation - 7.2-01 Cas par défaut

1. ETQ administrateur, je vais dans les paramètres de l'application, je vérifie que le champ Signature métier est vide.
2. Utiliser
   le [jeu de données de test](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/imports/donnees_test.sql)
3. Se connecter en tant que Notaire (dont le nom est "admin")
4. Faire une recherche avec les éléments suivants :
    * Date de décès : 01/01/2022
    * Prénom : Aimé
    * Nom obligatoire : Olmo
    * Date de naissance : 12/06/1977
5. Le mail reçu contient uniquement le texte suivant :

   ```text
   Maître,

   Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de aimé Olmo et dont le décès est survenu le : 01/01/2022.

   Cette personne est inconnue du département, vous trouverez ci-joint la réponse de mes services.

   Je vous prie d'agréer, Maître, mes courtoises salutations
   ```

### Tests d'acceptation - 7.2-01 Signature personnalisé

1. ETQ administrateur, je vais dans les paramètres de l'application, dans le champ *Signature métier*, j'ajoute:

   ```text
   --
   Conseil Départemental du Rhône
   https://www.rhone.fr/
   ```

2. Utiliser
   le [jeu de données de test](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/imports/donnees_test.sql)
3. Se connecter en tant que Notaire (dont le nom est "admin")
4. Faire une recherche avec les éléments suivants :
    * Date de décès : 01/01/2022
    * Prénom : aimé
    * Nom obligatoire : Olmo
    * Date de naissance : 12/06/1977
5. Le mail reçu contient uniquement le texte suivant :

   ```text
    Maître,

    Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de aimé Olmo et dont le décèsest survenu le : 01/01/2022.

    Cette personne est inconnue du département, vous trouverez ci-joint la réponse de mes services.

    Je vous prie d'agréer, Maître, mes courtoises salutations

    --
    Conseil Départemental du Rhône
    https://www.rhone.fr/
    ```

## 7.3 - Mails et PDF : disposer de balise pour tous les champs de la BD ===================================================

Ticket [Mails et PDF : disposer de balise pour tous les champs de la BD](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/129)
.

### User Story 7.3 Mails et PDF : disposer de balise pour tous les champs de la BD

* **ETQ :** opérateur technique d'une instance Départements & Notaires
* **Je souhaite :** disposer de variables reprenant tous les champs de la table INDIVIDU (`sexe`, `nom_usage`
  , `nom_civil`, `prenom`, `prenomd`, `prenomt`, `date_naissance`, `adresse`, `mdr`, `telephone`, `mail_mdr`, `libelle`
  , `code_aide`, `num_ind`)
* **Afin de :** afin de les utiliser dans les modèles de courriel ou courrier PDF

### Tests d'acceptation - 7.3-01 documentation des balises de modèles

1. ETQ opérateur technique, je trouve la documentation de toutes les balises utilisables dans les modèles dans le
   dossier [Documentation](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/tree/master/Documentation)
2. Par exemple, je m'attends à trouver (entre autres) une description de
    * `person.title`
    * `firstName`
    * `firstNameBdd`
    * `name`
    * `nameBdd`
    * `person.birth`
    * `birthDate`
    * `deathDate`
    * `pdfLogo1`
    * `pdfServiceLogo`
3. Les informations attendues sont :
    * quelles sont les valeurs possibles,
    * d'où viennent ces valeurs,
    * les éventuels traitements qui s'opèrent ou pourraient s'opérer dessus, etc

Les informations relatives à ces balises se trouvent dans le
fichier [Personnalisation.md](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/Documentation/Operateur/Personnalisation.md)
qui a été déplacé vers le dossier *Operateur*.

### Tests d'acceptation - 7.3-02 code et libellé de l'aide dans un courriel

1. ETQ opérateur technique, je personnalise le modèle de courriel pour ajouter le numéro d'individu et préciser le code
   et le libellé de l'aide
2. Se connecter en tant que Notaire (dont le nom est "admin")
3. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : aimé
    * Nom obligatoire : Olmo
    * Date de naissance : 06/12/1977
4. Le mail reçu par le notaire contient dans le corps du message :

   ```text
   Maître,

   Vous nous interrogez au sujet d'une éventuelle créance du département, au nom de Aimé Olmo né·e le 06/12/1977 et dont le décès est survenu le : 10/02/2020.

   Cette personne est connue du département, voici la liste de ses aides :

       - numéro d'individu : 1 ; libellé : Accueil PA en struct. collective ; code aide : 1SEXTREC

   Vous trouverez ci-joint la réponse de mes services.

   Je vous prie d'agréer, Maître, mes courtoises salutations
   ```

Si la personne est bénéficiaire de plusieurs aides, ces dernières doivent toutes être listées dans le message.
