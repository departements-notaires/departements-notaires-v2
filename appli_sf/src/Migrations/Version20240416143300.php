<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20240416143300 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Retire la contrainte mail unique sur la table membre ';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX UNIQ_F6B4FB295126AC48 ON membre');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('CREATE UNIQUE INDEX UNIQ_F6B4FB295126AC48 ON membre (mail)');
    }
}
