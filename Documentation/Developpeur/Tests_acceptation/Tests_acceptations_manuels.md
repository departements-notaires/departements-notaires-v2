# Tests d'acceptations manuels

## Informations

Il sera regroupé dans ce fichier, tous les tests d'acceptations qui n'ont pas été automatisés mais qui doivent tout de
mêmes être déroulés pour s'assurer du bon fonctionnement de l'application.

Pour le moment ces tests ne concernent que la v2.

## 1. Statistiques

Ce test fait référence au test d'acceptation *6.3-03-A statistiques* du
ticket [#149](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/149)

1. Se connecter en tant que Agent ou Administrateur
2. Faire une recherche avec les éléments suivants :
    - Date décès : `10/02/2020`
    - Lieu de décès : `VILLEFRANCHE`
    - Date du certificat de décès : `12/02/2020`
    - Prénom : `Arno`
    - Nom : `Nimousse`
    - Date de naissance : `01/01/1920`
3. Se rendre dans statistiques
4. Le tableau doit afficher une ligne Aides en cours d'instruction sous la ligne Indus probables *(Ce point est
   représenté dans un test cypress)*
5. Le graphique doit présenter aussi les Aides en cours d'instruction

Résultat attendu :

- Le graphique d'évolutions des demandes situé dans la page de Statistiques contient `Aide en cours d'instruction` dans
  sa légende
- la couleur d'aide en cours d'instruction représente 1/3 de barre représantant les type d'aides dans le graphique.

## 2. Recherche

Ces tests font références aux tests d'acceptation *6.1. Import : gérer les bénéficiaires de plusieurs aides* du
ticket [#147](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/147)
Pour tous les tests suivants les tests de statut sur la page du site, ainsi que les tests de mails sont déjà réalisés
dans des tests cypress. Ici seul le contenu des pdf envoyé sera traité.

### Tests d'acceptation : Personne existente avec prénom composée écrit dans le mauvais sens donne ambigü, aide non récupérable

1. Se connecter en tant que Admin
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Gaël
    * Nom : PAYSAA CAMPO
    * Date de naissance : 16/04/2008

Résultat attendu :

- La page de résultat de recheche affiche 'Les éléments que vous avez saisis ne permettent pas d'établir avec certitude
  l'identité'.
- Le mail reçu contient dans son contenu 'Les éléments suivants ne permettent pas d'établir avec certitude l'identité de
  la personne.'

### Tests d'acceptation - 6.1-2 Cas "B1" : Recherche de bénéficiaire avec 2 aides (une non récupérable et une récupérable)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Lucette
    * Nom : Perron
    * Date de naissance : 01/01/1905

Résultat attendu :

- Le PDF (en pièce jointe du courriel) contient : `1 aide récupérable` et `1 aide non récupérable`
- Le PDF (en pièce jointe du courriel) ne contient pas "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "B2" : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables et 1 récupérable)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Janine
    * Nom : Peyroul
    * Date de naissance : 01/01/1910

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient : `2 aides récupérables` et `1 aide non récupérable`
- Le PDF (en pièce jointe du courriel) ne contient pas "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "B3" : Recherche de bénéficiaire avec plusieurs aides (1 non récupérable et 2 récupérables)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Patricia
    * Nom : Patate
    * Date de naissance : 01/01/1911

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient : `1 aide récupérable` et `2 aides non récupérables`
- Le PDF (en pièce jointe du courriel) ne contient pas "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "B4" : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables et 2 récupérables)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Dalal
    * Nom : Syrie
    * Date de naissance : 01/01/1912

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient : `2 aides récupérables` et `2 aides non récupérables`

1. Le PDF (en pièce jointe du courriel) ne contient pas "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "C1" : Recherche de bénéficiaire avec 1 aide non récupérables

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Maesson
    * Nom : ALOUANE
    * Date de naissance :  03/12/1954

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient : `1 aide non récupérable`
- Le PDF (en pièce jointe du courriel) ne contient pas "0 aide récupérable", ni "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "C2" : Recherche de bénéficiaire avec 2 aides non récupérables

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : Bordeau
    * Date du certificat de décès : 12/02/2020
    * Prénom : Francine
    * Nom : de la Rochebonnefoy
    * Date de naissance : 01/01/1903

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient : `2 aides non récupérables`
- Le PDF (en pièce jointe du courriel) ne contient pas "0 aide récupérable", ni "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "D1" : Recherche de bénéficiaire avec 1 aide récupérable

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Aimé
    * Nom : OLMO
    * Date de naissance : 06/12/1977

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient : `1 aide récupérable`
- Le PDF (en pièce jointe du courriel) ne contient pas "0 aide non récupérable", ni "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "D2" : Recherche de bénéficiaire avec 2 aides récupérables

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Pierre
    * Nom : Mayou
    * Date de naissance : 01/01/1904

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient : `2 aides récupérables`
- Le PDF (en pièce jointe du courriel) ne contient pas "0 aide non récupérable", ni "0 aide en cours d'instruction"

### Tests d'acceptation - 6.1-2 Cas "E1" : Recherche de bénéficiaire avec 1 aide en cours d'instruction

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Berthe
    * Nom : Goitreux
    * Date de naissance : 01/01/1908

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient : `1 aide en cours d'instruction`
- Le PDF (en pièce jointe du courriel) ne contient pas "0 aide récupérable", ni "0 aide non récupérable"

### Tests d'acceptation - 6.1-2 Cas "E2" : Recherche de bénéficiaire avec 2 aides en cours d'instruction

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Émile
    * Nom : Carabignac
    * Date de naissance : 01/01/1909

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient : `2 aides en cours d'instruction`
- Le PDF (en pièce jointe du courriel) ne contient pas "0 aide récupérable", ni "0 aide non récupérable"

### Tests d'acceptation - 6.1-2 Cas "F1"

Cas "F1" :Recherche de bénéficiaire avec 3 aides (1 non récupérable, 1 récupérable et 1 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Bastien
    * Nom : Legoumier
    * Date de naissance : 01/01/1919

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient :
    - `1 aide récupérable`
    - `1 aide non récupérable`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "F2"

Cas F2 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables, 2 récupérables et 1 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Jean-Chi
    * Nom : Moutardier
    * Date de naissance : 01/01/1921

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient :
    - `2 aides récupérables`
    - `2 aides non récupérables`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas F3

Cas F3 : Recherche de bénéficiaire avec plusieurs aides (1 non récupérable, 2 récupérables et 1 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Etienne
    * Nom : Buset
    * Date de naissance : 01/01/1922

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient :
    - `1 aide récupérable`
    - `2 aides non récupérables`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "F4"

Cas F4 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables, 1 récupérable et 1 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Blandine
    * Nom : Apatou
    * Date de naissance : 01/01/1923

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient :
    - `2 aides récupérables`
    - `1 aide non récupérable`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas F5

Cas F5 : Recherche de bénéficiaire avec plusieurs aides (1 non récupérable, 1 récupérable et 2 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Clémence
    * Nom : Kousou
    * Date de naissance : 01/01/1924

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient :
    - `1 aide récupérable`
    - `1 aide non récupérable`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas F6

Cas F6 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables, 2 récupérables et 2 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Arno
    * Nom : Nimousse
    * Date de naissance : 01/01/1920

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient :
    - `2 aides récupérables`
    - `2 aides non récupérables`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas F7

Cas F7 : Recherche de bénéficiaire avec plusieurs aides (1 non récupérable, 2 récupérables et 2 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Bernard
    * Nom : Perron
    * Date de naissance : 01/01/1925

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient :
    - `1 aide récupérable`
    - `2 aides non récupérable`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas F8

Cas F8 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables, 1 récupérable et 2 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Anita
    * Nom : Bonny
    * Date de naissance : 01/01/1926

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) contient :
    - `2 aides récupérables`
    - `1 aide non récupérable`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas G1

Cas G1 : Recherche de bénéficiaire avec 2 aides (une non récupérable et une en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Sophie
    * Nom : Perron
    * Date de naissance : 01/01/1907

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) ne contient pas `0 aide récupérable`
- Le PDF (en pièce jointe du courriel) contient :
    - `1 aide non récupérable`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "G2"

Cas G2 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables et 1 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Vasco
    * Nom : Boisséson
    * Date de naissance : 01/01/1913

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) ne contient pas `0 aide récupérable`
- Le PDF (en pièce jointe du courriel) contient :
    - `2 aides non récupérables`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "G3"

Cas G3 : Recherche de bénéficiaire avec plusieurs aides (1 non récupérable et 2 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Joseph
    * Nom : Couvreur
    * Date de naissance : 01/01/1914

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) ne contient pas `0 aide récupérable`
- Le PDF (en pièce jointe du courriel) contient :
    - `1 aide non récupérable`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "G4"

Cas G4 : Recherche de bénéficiaire avec plusieurs aides (2 non récupérables et 2 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Armand
    * Nom : Garan-Servier
    * Date de naissance : 01/01/1915

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) ne contient pas `0 aide récupérable`
- Le PDF (en pièce jointe du courriel) contient :
    - `2 aides non récupérables`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "H1"

Cas H1 : Recherche de bénéficiaire avec 2 aides (une récupérable et une en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Antoine
    * Nom : Perron
    * Date de naissance : 01/01/1906

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) ne contient pas `0 aide non récupérable`
- Le PDF (en pièce jointe du courriel) contient :
    - `1 aide récupérable`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "H2"

Cas H2 : Recherche de bénéficiaire avec plusieurs aides (2 récupérables et 1 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Pierre-Henri
    * Nom : Garan-Servier
    * Date de naissance : 01/01/1916

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) ne contient pas "0 aide non récupérable"
- Le PDF (en pièce jointe du courriel) contient :
    - `2 aides récupérables`
    - `1 aide en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas "H3"

Cas H3 : Recherche de bénéficiaire avec plusieurs aides (1 récupérable et 2 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Enguerrand
    * Nom : Brousse
    * Date de naissance : 01/01/1917

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) ne contient pas `0 aide non récupérable`
- Le PDF (en pièce jointe du courriel) contient :
    - `1 aide récupérable`
    - `2 aides en cours d'instruction`

### Tests d'acceptation - 6.1-2 Cas H4

Cas H4 : Recherche de bénéficiaire avec plusieurs aides (2 récupérables et 2 en cours d'instruction)

1. Se connecter en tant que Notaire
1. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : François
    * Nom : Civrac
    * Date de naissance : 01/01/1918

Résultat obtenu :

- Le PDF (en pièce jointe du courriel) ne contient pas "0 aide non récupérable"
- Le PDF (en pièce jointe du courriel) contient :
    - `2 aides récupérables`
    - `2 aides en cours d'instruction`

## 3. Mails

Ces tests font références aux tests d'acceptation *7.1. Pouvoir personnaliser le sujet d’un mail* du
ticket [#127](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/127)

### Test d'acceptation - 7.1-01 CONNU cas par défaut

1. Se connecter en tant que Notaire (dont le nom est "admin")
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : aimé
    * Nom obligatoire : Olmo
    * Date de naissance : 06/12/1977

Résultat obtenu :

- Le sujet du mail sera `[Départements et Notaires] admin - Succession Aimé Olmo - connu`

### Tests d'acceptation - 7.1-02 CONNU cas personnalisé

1. ETQ qu'opérateur technique, je modifie le modèle de sujet de mail pour une personne connue
2. J'y place le contenu: `[{{ appName }}] {{ status }} - {{ userName }} - Succession {{ firstName }} {{ useName }}`
3. Se connecter en tant que Notaire (dont le nom est "admin")
4. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : aimé
    * Nom obligatoire : Olmo
    * Date de naissance : 06/12/1977

Résultat obtenu :

- Le sujet du mail sera `[Départements et Notaires] connu - admin - Succession Aimé Olmo`

### Test d'acceptation - 7.1-03 INCONNU cas par défaut

1. Se connecter en tant que Notaire (dont le nom est "admin")
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Mickael
    * Nom obligatoire : Dupont
    * Date de naissance : 07/08/1986

Résultat obtenu :

- Le sujet du mail sera `[Départements et Notaires] admin - Succession Mickael Dupont - inconnu`

### Tests d'acceptation - 7.1-04 INCONNU cas personnalisé

1. ETQ qu'opérateur technique, je modifie le modèle de sujet de mail pour une personne inconnue
2. J'y place le contenu: `[{{ appName }}] {{ status }} - {{ userName }} - Succession {{ firstName }} {{ useName }}`
3. Se connecter en tant que Notaire (dont le nom est "admin")
4. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Mickael
    * Nom obligatoire : Dupont
    * Date de naissance : 07/08/1986

Résultat obtenu :

- Le sujet du mail sera `[Départements et Notaires] inconnu - admin - Succession Mickael Dupont`

### Test d'acceptation - 7.1-05 EN COURS D'INSTRUCTION cas par défaut

1. Se connecter en tant que Notaire (dont le nom est "admin")
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Yvette
    * Nom obligatoire : ROSA
    * Date de naissance : 08/12/1968

Résultat obtenu :

- Le sujet du mail sera `[Départements et Notaires] admin - Succession Yvette ROSA - en cours d'instruction`

### Tests d'acceptation - 7.1-06 EN COURS D'INSTRUCTION cas personnalisé

1. ETQ qu'opérateur technique, je modifie le modèle de sujet de mail pour une personne en cours d'instruction
2. J'y place le contenu: `[{{ appName }}] {{ status }} - {{ userName }} - Succession {{ firstName }} {{ useName }}`
3. Se connecter en tant que Notaire (dont le nom est "admin")
4. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Yvette
    * Nom obligatoire : ROSA
    * Date de naissance : 08/12/1968

Résultat obtenu :

- Le sujet du mail sera `[Départements et Notaires] en cours d'instruction - admin - Succession Yvette ROSA`

### Test d'acceptation - 7.1-07 AMBIGU cas par défaut

1. Se connecter en tant que Notaire (dont le nom est "admin")
2. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Clara
    * Nom obligatoire : BOURDIAU
    * Date de naissance : 01/09/1966

Résultat obtenu :

- Le sujet du mail sera `[Départements et Notaires] admin - Succession Clara BOURDIAU - ambigu`

### Tests d'acceptation - 7.1-08 AMBIGU cas personnalisé

1. ETQ qu'opérateur technique, je modifie le modèle de sujet de mail pour une personne ambiguë
2. J'y place le contenu: `[{{ appName }}] {{ status }} - {{ userName }} - Succession {{ firstName }} {{ useName }}`
3. Se connecter en tant que Notaire (dont le nom est "admin")
4. Faire une recherche avec les éléments suivants :
    * Date décès : 10/02/2020
    * Lieu de décès : VILLEFRANCHE
    * Date du certificat de décès : 12/02/2020
    * Prénom : Clara
    * Nom obligatoire : BOURDIAU
    * Date de naissance : 01/09/1966

Résultat obtenu :

- Le sujet du mail sera `[Départements et Notaires] ambigu - admin - Succession Clara BOURDIAU`

### Tests d'acceptation - 6.3-1 historique de recherche Notaire

Ce test fait référence au test d'acceptation *6.3-1 historique de recherche Notaire* du
ticket [#149](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/issues/149)

1. Se connecter en tant que notaire
2. Faire une recherche avec les éléments suivants :
    - Date décès : 10/02/2020
    - Lieu de décès : VILLEFRANCHE
    - Date du certificat de décès : 12/02/2020
    - Prénom : Arno
    - Nom : Nimousse
    - Date de naissance : 01/01/1920
3. Se rendre dans Mes précédentes recherches

Résultat obtenu :

- Le champ Type de réponse doit proposer (en plus des valeurs actuelles), la valeur *Aide en cours d'instruction*
- Dans le tableau, à la ligne correspondant à la recherche, la colonne *Réponse* doit afficher les 3 résultats i.e. :
    - Récupération
    - Indus probable
    - Aide en cours d'instruction
