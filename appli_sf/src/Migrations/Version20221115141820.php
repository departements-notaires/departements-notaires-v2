<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20221115141820 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add a many to many relation between Claim and Person tables';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE creances_individus (id_creance INT NOT NULL, id_individu INT NOT NULL, CONSTRAINT PK_194DORIHRLWMNEXA PRIMARY KEY(id_creance, id_individu)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE creances_individus');
    }
}
