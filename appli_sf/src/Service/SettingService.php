<?php

namespace App\Service;

use App\Entity\Settings;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SettingService.
 */
class SettingService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @return string
     */
    public function getValue(string $name)
    {
        /** @var Settings|null $setting */
        $setting = $this->em->getRepository(Settings::class)->findOneBy(['name' => $name]);

        if (null !== $setting) {
            return $setting->getValue();
        }

        return '';
    }
}
