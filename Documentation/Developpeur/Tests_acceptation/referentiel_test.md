# STRUCTURE DES TESTS

[[_TOC_]]

Nous avons identifié 3 critères fondamentaux de l'application Département & Notaires. Ceux-ci, selon la combinaison
employée, peuvent impacter le résultat d'une recherche effectuée par un notaire. Les critères sont les suivants :

1. **La configuration utilisée par le Département**
2. **La saisie de recherche effectuée par une Étude**
3. **Le résultat de la recherche transmis par l'application D&N**

Le référentiel suivant a donc pour but de détailler :

- Les combinaisons de paramétrages existant dans D&N.
- Les types d'aides et les informations saisies.
- les résultats d'une recherche.

Il permettra de tendre à une exhaustivité des cas de figures qui seront amenés à être présents dans
l'application Département & Notaires et de mieux comprendre son utilisation.

## Critère 1 : La configuration utilisée

Nous avons établi que trois éléments de la configuration permettent de modifier un résultat attendu. Ce sont :

1. L'activation ou non de la recherche soundex (P1)
2. La précision de la date (P2.0 à 2.4)
3. Le prénom différent donne ambigu au lieu de connu (P3)

Sur ces spécifications, il est possible d'arriver à une combinaison de 20 paramétrages différents.

Configuration par défaut :

- Coche soundex désactivée
- Critère de date sur jour et mois donnent ambigus
- Coche prénom différent donne ambigu au lieu de connu désactivée

Cette valeur se nommera **Conf+numéro de la config de 0 à 19** (0 étant la configuration par défaut de l'application).

De ce fait, la déclinaison proposée est la suivante :

| Code config | Élément modifié par rapport à la config par défaut | Descriptif                                                     |
|-------------|----------------------------------------------------|----------------------------------------------------------------|
| Conf00      | /                                                  | config par défaut                                              |
| Conf01      | P1                                                 | coche soundex activée                                          |
| Conf02      | P3                                                 | coche prénom activée                                           |
| Conf03      | P2.1                                               | coche Jour                                                     |
| Conf04      | P2.2                                               | coche Mois                                                     |
| Conf05      | P2.3                                               | coche Année                                                    |
| Conf06      | P2.4                                               | coche Date                                                     |
| Conf07      | P1 + P3                                            | coches soundex **ET** prénom activées                          |
| Conf08      | P1 + P2.1 + P3                                     | coches soundex **ET** coche Jour **ET** coche prénom activées  |
| Conf09      | P1 + P2.2 + P3                                     | coches soundex **ET** coche Mois **ET** coche prénom activées  |
| Conf10      | P1 + P2.3 + P3                                     | coches soundex **ET** coche Année **ET** coche prénom activées |
| Conf11      | P1 + P2.4 + P3                                     | coches soundex **ET** coche Date **ET** coche prénom activées  |
| Conf12      | P1 + P2.1                                          | coches soundex **ET** coche Jour activées                      |
| Conf13      | P1 + P2.2                                          | coches soundex **ET** coche Mois activées                      |
| Conf14      | P1 + P2.3                                          | coches soundex **ET** coche Année activées                     |
| Conf15      | P1 + P2.4                                          | coches soundex **ET** coche Date activées                      |
| Conf16      | P2.1  + P3                                         | coche Jour **ET** coche prénom activées                        |
| Conf17      | P2.2  + P3                                         | coche Mois **ET** coche prénom activées                        |
| Conf18      | P2.3  + P3                                         | coche Année **ET** coche prénom activées                       |
| Conf19      | P2.4  + P3                                         | coche Date **ET** coche prénom activées                        |

### Détermination de la configuration par défaut

#### Paramètre `Recherche soundex`

* Valeur par défaut : `false`
* Origine de l'information dans le code source :
    * [SettingsData.php:61](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/src/Data/SettingsData.php#L61)

#### Paramètre `Précision de la date de naissance`

* Valeur par défaut : `Jour et mois différents donnent ambigu au lieu d'inconnu`
* Origine de l'information dans le code source :
    * [SettingsData.php:82](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/src/Data/SettingsData.php#L82)
    * [SearchHelper.php:22](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/src/Helper/SearchHelper.php#L22)
    * [messages.fr.yaml:69](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/translations/messages.fr.yaml#L69)

#### Paramètre `Prénom différent donne ambigu au lieu de connu`

* Valeur par défaut : `false`
* Origine de l'information dans le code source :
    * [SettingsData.php:67](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/src/Data/SettingsData.php#L67)

#### Paramètre `Recherche par nom`

* Valeur par défaut : `Nom d'usage`
* Origine de l'information dans le code source :

    * [SearchHelper.php:19](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/src/Helper/SearchHelper.php#L19)
    * [SettingsData.php:77](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/src/Data/SettingsData.php#L77)
    * [messages.fr.yaml:76](https://gitlab.adullact.net/departements-notaires/departements-notaires-v2/-/blob/master/appli_sf/translations/messages.fr.yaml#L76)

## Critère 2 : Les résultats de recherche

Il existe dans l'application 3 résultats de recherche distincts :

1. **Personne inconnue**
2. **Personne connue**
3. **Personne ambigue**

Avec l'apparition de l'aide multiple, les cas de figures augmentent le nombre de possibilités de résultat
du cas **connu** :

- Personne connue sans aucune aide
- Personne connue avec une aide récupérable
- Personne connue avec une aide non récupérable
- Personne connue avec une aide en cours d'instruction

(N.B. : à décliner d'autant plus qu'un bénéficiaire peut bénéficier de plusieurs aides récupérables ou non)

Ce référentiel sera numéroté de **A à J + un chiffre** selon les possibilités de cas rencontrés

De ce postulat, cela donne :

| Code résultat | Résultat de la recherche                                                                                              |
|---------------|-----------------------------------------------------------------------------------------------------------------------|
| A1            | Recherche de bénéficiaire CONNU sans aucune aide                                                                      |
| B1            | Recherche de bénéficiaire CONNU avec 2 aides (une non récupérable et une récupérable)                                 |
| B2            | Recherche de bénéficiaire CONNU avec plusieurs aides (2 récupérables et 1 non-récupérable)                            |
| B3            | Recherche de bénéficiaire CONNU avec plusieurs aides (1 récupérable et 2 non-récupérables)                            |
| B4            | Recherche de bénéficiaire CONNU avec plusieurs aides (2 non récupérables et 2 récupérables)                           |
| C1            | Recherche de bénéficiaire CONNU avec 1 aide non récupérable                                                           |
| C2            | Recherche de bénéficiaire CONNU avec 2 aides non récupérables                                                         |
| D1            | Recherche de bénéficiaire CONNU avec 1 aide récupérable                                                               |
| D2            | Recherche de bénéficiaire CONNU avec 2 aides récupérables                                                             |
| E1            | Recherche de bénéficiaire CONNU avec 1 aide en cours d'instruction                                                    |
| E2            | Recherche de bénéficiaire CONNU avec 2 aides en cours d'instruction                                                   |
| F1            | Recherche de bénéficiaire CONNU avec 3 aides (1 non récupérable, 1 récupérable et 1 en cours d'instruction)           |
| F2            | Recherche de bénéficiaire CONNU avec plusieurs aides (2 non récupérables, 2 récupérables et 1 en cours d'instruction) |
| F3            | Recherche de bénéficiaire CONNU avec plusieurs aides (1 non récupérable, 2 récupérables et 1 en cours d'instruction)  |
| F4            | Recherche de bénéficiaire CONNU avec plusieurs aides (2 récupérables, 1 non récupérable et 1 en cours d'instruction)  |
| F5            | Recherche de bénéficiaire CONNU avec plusieurs aides (1 non récupérable, 1 récupérable et 2 en cours d'instruction)   |
| F6            | Recherche de bénéficiaire CONNU avec plusieurs aides (2 non récupérables, 2 récupérables et 2 en cours d'instruction) |
| F7            | Recherche de bénéficiaire CONNU avec plusieurs aides (1 récupérable, 2 non récupérables et 2 en cours d'instruction)  |
| F8            | Recherche de bénéficiaire CONNU avec plusieurs aides (2 récupérables, 1 non récupérable et 2 en cours d'instruction)  |
| G1            | Recherche de bénéficiaire CONNU avec 2 aides (une non récupérable et une en cours d'instruction)                      |
| G2            | Recherche de bénéficiaire CONNU avec plusieurs aides (2 non récupérables et 1 en cours d'instruction)                 |
| G3            | Recherche de bénéficiaire CONNU avec plusieurs aides (1 non récupérable et 2 en cours d'instruction)                  |
| G4            | Recherche de bénéficiaire CONNU avec plusieurs aides (2 non récupérables et 2 en cours d'instruction)                 |
| H1            | Recherche de bénéficiaire CONNU avec 2 aides (une récupérable et une en cours d'instruction)                          |
| H2            | Recherche de bénéficiaire CONNU avec plusieurs aides (2 récupérables et 1 en cours d'instruction)                     |
| H3            | Recherche de bénéficiaire CONNU avec plusieurs aides (1 récupérable et 2 en cours d'instruction)                      |
| H4            | Recherche de bénéficiaire CONNU avec plusieurs aides (2 récupérables et 2 en cours d'instruction)                     |
| I1            | Recherche de bénéficiaire INCONNU                                                                                     |
| J1            | Recherche de bénéficiaire AMBIGU                                                                                      |

## Critère 3 : la saisie de recherche

### Les noms et prénoms et les cas complexes

La recherche Soundex et la recherche prénom différent donne ambigu permettent de mettre au jour plusieurs cas de figure
qui peuvent survenir à l'état civil.

En effet, s'il existe des noms et prénoms relativement simples, le niveau de complexité de la requête augmente
avec les noms (civils ou d'usage) et prénoms composés de par leur saisie dans la base de données.
Nous avons identifié plusieurs cas de figures :

1. Le cas d'un nom de famille composé sans un tiret (Nom A Nom B)
2. Le cas d'un nom de famille composé avec un tiret (Nom A-Nom B)
3. Le cas d'un prénom composé sans un tiret
4. Le cas d'un prénom composé avec un tiret

Ce critère est donc important afin de renforcer l'idée qu'un résultat doit être identique si un individu est composé
d'un nom avec tiret ou sans.

Voici le référentiel en conséquence structuré de la lettre **N ou P en fonction du nom ou du prénom et de CST
(composé sans tiret) et CT (composé avec tiret)**

| Code Identité | Type de NOM            |
|---------------|------------------------|
| N             | Nom                    |
| NCST          | Nom composé sans tiret |
| NCT           | Nom composé avec tiret |

Il en est de même avec le PRÉNOM :

| Code Identité | Type de PRÉNOM            |
|---------------|---------------------------|
| P             | Prénom                    |
| PCST          | Prénom composé sans tiret |
| PCT           | Prénom composé avec tiret |

### Exemples de structure des NOMS de tests d'acceptation

## LE CUMUL DES 3 CRITÈRES

Chaque test sera référencé dans un fichier qui portera son nom sous la forme de nomenclature des 3 référentiels
combinés.

De ce fait, il sera plus facile de savoir à chaque mise à jour de version si un test en particulier à été défaillant.

Dans chaque fichier sera décrit la recherche effectuée ainsi que le résultat attendu.

Les fichiers seront donc structurés de cette manière et séparés par des tirets :

1. en premier le Code Config
2. suivi par le Code Résultat
3. puis par le Code Identité du NOM
4. et pour finir par le Code Identité du PRÉNOM

...ce qui apparaîtra sous la forme :

```text
CodeConfig-CodeRésultat-CodeIdentitéNOM-CodeIdentitéPRÉNOM
```

### Exemples

#### Exemple 1 `Conf0-A1-N-PCT`

Qui se comprend par :

- Config par défaut
- bénéficiaire connu sans aucune aide
- avec un nom classique et un prénom composé avec tiret

#### Exemple 2 `Conf19-H2-NCST-P`

Qui se comprend par :

- Config COCHE DATE ET COCHE PRÉNOM ACTIVÉES
- bénéficiaire connu avec plusieurs aides (2 récupérables et 2 en cours d'instruction)
- avec un nom composé sans tiret et un prénom classique

#### Autres cas

```shell
CONF0-A1-N-P
CONF0-A1-N-PCSTS
CONF0-A1-N-PCT
CONF0-A1-NCST-P
CONF0-A1-NCST-PCST
CONF0-A1-NCST-PCT
CONF0-A1-NCT-P
CONF0-A1-NCT-PCST
```

## Modèle des tests d'acceptation

### Nomenclature test d'acceptation

#### Test d'acceptation `Conf-H-N-P` (nom de la configuration)

##### Pré-requis

L'application doit être dans la [configuration XX](../Configurations.md#ConfXX)

##### Saisie du test

- Se connecter en tant qu'étude 3
- Aller sur l'onglet recherche et saisir les valeurs suivantes :
    - Date de décès : 10/02/2020
    - Lieu de décès : VILLEFRANCHE
    - Date de l'acte de décès : 10/02/2020
    - Prénom :
    - Nom d'usage :
    - Date de naissance :
- La page résultat de recherche contient :
- L'Affichage dans le tableau mes précédentes recherches contient :
- Template mail généré est :
- Le courriel reçu par le Notaire contient :
- Le courriel reçu par le Notaire contient :
- Le courriel reçu par le Notaire ne contient pas :
- Template PDF généré est :
- Le PDF (en pièce jointe du courriel) contient :
- Le PDF (en pièce jointe du courriel) ne contient pas :
