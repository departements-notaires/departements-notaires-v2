<?php

namespace App\Entity;

use App\Repository\UserLogRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

#[ORM\Entity(repositoryClass: UserLogRepository::class)]
#[ORM\Table(name: 'log_membre')]
class UserLog
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[ORM\Column(name: 'date', type: 'datetime')]
    private $date;

    #[ORM\ManyToOne(targetEntity: "App\Entity\User", inversedBy: 'userLogs')]
    #[ORM\JoinColumn(name: 'id_membre', options: ['nullable' => false, ' onDelete' => 'SET NULL'])]
    private $user;

    #[ORM\Column(name: 'echec', type: 'boolean')]
    private $failure;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(User|UserInterface|null $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getFailure(): ?bool
    {
        return $this->failure;
    }

    public function setFailure(bool $failure): self
    {
        $this->failure = $failure;

        return $this;
    }
}
