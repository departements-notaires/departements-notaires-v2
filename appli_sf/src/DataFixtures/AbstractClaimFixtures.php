<?php

namespace App\DataFixtures;

use App\Entity\Claim;
use App\Repository\PersonRepository;
use Doctrine\Persistence\ObjectManager;

abstract class AbstractClaimFixtures extends \Doctrine\Bundle\FixturesBundle\Fixture
{
    /**
     * @var PersonRepository
     */
    private $personRepository;

    public function __construct(PersonRepository $personRepository)
    {
        $this->personRepository = $personRepository;
    }

    public function load(ObjectManager $manager)
    {
        $header = null;
        $filename = __DIR__.'/../../imports/creances.csv';
        $data = [];

        if (false !== ($handle = fopen($filename, 'r'))) {
            while (false !== ($row = fgetcsv($handle, 1000, ';'))) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }

            fclose($handle);
        }

        // Permet de changer le statut (récupérable/non récupérable) à partir de la deuxième boucle
        $claimNumIndCount = [];

        foreach ($data as $claimData) {
            $claim = new Claim();

            $recoverableValue = in_array($claimData['num_ind'], $claimNumIndCount);
            $person = $this->personRepository->findOneBy(['personNum' => $claimData['num_ind']]);

            $claim->addPerson($person)
                  ->setNumInd(intval($claimData['num_ind']))
                  ->setStartDate($this->getConvertedDate($claimData['date_deb_periode']))
                  ->setEndDate($this->getConvertedDate($claimData['date_fin_periode']))
                  ->setAmount(floatval(trim($claimData['montant'])))
                  ->setCalculatedDate($this->getConvertedDate($claimData['date_calcul']))
                  ->setDisplayable(boolval(trim($claimData['affichable'])))
                  ->setRecoverable($recoverableValue);

            $claimNumIndCount[] = $claim->getNumInd();

            $manager->persist($claim);
        }

        $manager->flush();
    }

    public function getConvertedDate($claimData)
    {
        return !empty($claimData) ? \DateTime::createFromFormat('Y-m-j', trim($claimData)) : null;
    }
}
