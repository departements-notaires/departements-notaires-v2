<?php

namespace App\Service;

use App\Data\SettingsData;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class SettingService.
 */
class SettingsDataService
{
    /**
     * @var SettingsData
     */
    private $settingsData;

    /**
     * SettingDataService constructor.
     *
     * @throws \ReflectionException
     */
    public function __construct(SettingsData $settingsData, EntityManagerInterface $entityManager)
    {
        $this->settingsData = $settingsData->initFromDatabase($entityManager);
    }

    /**
     * Récupère les paramètres du site.
     */
    public function getData(): SettingsData
    {
        return $this->settingsData;
    }
}
